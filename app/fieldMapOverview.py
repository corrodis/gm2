import gm2
from gm2 import plt, np

temp = True
show = True




import os
if not os.path.isfile("trolleyRuns.txt"):
    print("Couldn't fine `trolleyRuns.txt`")
    import sys
    sys.exit()

def getRunList():
      runs = np.loadtxt("trolleyRuns.txt")
      return np.array([int(run) for run in runs])

t = gm2.Temperature(db=True)


avgs = []
rmss = []

for run in getRunList():
    print run
    if run in [7253]:
        fm = gm2.FieldMap([run], load='simple', useEncoder=True)
    else:
        fm = gm2.FieldMap([run], load='simple', useEncoder=True)
    avgs.append(fm.avg())
    rmss.append(fm.rms())

r = gm2.Runs()
times = r.times(getRunList())
ts = np.array([gm2.util.datetime2ts_dt(dd[0]) for dd in times])
rmss = np.array(rmss)
avgs = np.array(avgs)

pp = 300
if temp:
    t.load(start=times[0][0], end=times[-1][1])
    pp = 401


figsize = gm2.plotutil.figsize()
f = plt.figure(figsize=[figsize[0]*1.5, figsize[1]*2])
ax1 = plt.subplot(11 + pp)
gm2.plotutil.plot_ts(ts,  avgs[:,0], 'o')
#ax1.plot([dd[0] for dd in times], avgs[:,0], 'o')
ax1.set_ylabel("Avg. Dipole [Hz]")
gm2.plotutil.plot_ts(ts[[0,-1]],  [51000.,51000.], '--', color="grey", alpha=0.5)
ax1.set_ylim([50900, 51100])
ax2 = plt.subplot(12 + pp, sharex=ax1)
gm2.plotutil.plot_ts(ts, rmss[:,0]*gm2.HZ2PPM)
#ax2.plot([dd[0] for dd in times], rmss[:,0]*gm2.HZ2PPM)
ax2.set_ylabel("Dipole RMS [ppm]")
ax3 = plt.subplot(13 + pp, sharex=ax1)

mp_names = ["NQ", "SQ", "NS", "SS", "NO", "SO", "ND", "SD"]

for i in range(1,9):
    gm2.plotutil.plot_ts(ts, avgs[:,i]*gm2.HZ2PPM, label=mp_names[i-1])
#ax3.plot([dd[0] for dd in times], avgs[:,1:]*gm2.HZ2PPM)
ax3.set_ylabel("Avg Multipoles [ppm]")
ax3.set_ylim([-1.5,1.5])
ax3.legend(ncol=4, fontsize = 'x-small')
plt.setp( ax1.get_xticklabels(), visible=False)
plt.setp( ax2.get_xticklabels(), visible=False)
if temp:
    ax4 = plt.subplot(10 + pp, sharex=ax1)
    gm2.plotutil.plot_ts(np.array(t.data['EastWall']['South']['times']), np.array(t.data['EastWall']['South']['values']),'.', markersize=2, label="Hall")
    #gm2.plotutil.plot_ts(np.array(t.data['Outside']['ES&H']['times']), np.array(t.data['Outside']['ES&H']['values']),'.', markersize=2, label="Outside")
    gm2.plotutil.plot_ts(np.array(t.data['D']['Top']['times']), np.array(t.data['D']['Top']['values']),'.', markersize=2, label="Yoke D")
    gm2.plotutil.plot_ts(np.array(t.data['K']['Vac']['times']), np.array(t.data['K']['Vac']['values']),'.', markersize=2, label="Vacuum K")
    gm2.plt.legend(markerscale=4, fontsize='x-small')
    ax4.set_ylabel("Temperature [C]")
    plt.setp( ax4.get_xticklabels(), visible=False)

gm2.despine()
f.savefig("plots/trolleyOverview.png")
if show:
   f.show()
print("plots/trolleyOverview.png")

if True:
    markers= ["v","^","<",">","p", "*", "X", "D"]
    f = plt.figure(figsize=[figsize[0]*1.5, figsize[1]*2])
    for i in range(1,9):
        gm2.plotutil.plot_ts(ts, avgs[:,i]*gm2.HZ2PPM, label=mp_names[i-1], marker=markers[i-1], alpha=0.9)
    plt.ylabel("Avg Multipoles [ppm]")
    plt.ylim([-1.5,1.5])
    plt.legend()
    gm2.despine()
    plt.show()


