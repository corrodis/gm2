# package gm2
# module fixedprobe
# Author: Simon Corrodi
#         scorrodi@anl.gov
#         September 2018


import numpy as np
from gm2 import rootbase, util

class FixedProbe(rootbase, object):
    def __init__(self, runs = [], load=False, prefix=None, DAQ=False, mode_freq=0):
        self.runs = runs
        self.loadSettings(DAQ)
        self.qtag = None
        super(FixedProbe, self).__init__('fixedProbe',prefix=prefix)
        self.loadFiles()
        if load:
            self.loadBasicMode(mode_freq=mode_freq)
        #self.loadJs()

    def loadJs(self, discardMixing=False):
        self.J4  = np.array([[1.0,  0.0,  0.0,  0.0],
                             [0.0,  1.0,  0.0,  0.0],
                             [0.0,  0.0,  1.0, -0.33],
                             [0.0,  0.0,  0.0,  0.5]])

        self.J4g = np.array([[1.0,  0.0,  0.0,  0.0],
                             [0.0,  1.0,  0.0,  0.0],
                             [0.0,  0.19, 1.0,  0.0],
                             [0.0,  0.0,  0.0,  0.5]]) 

        self.J6  = np.array([[1.0,  0.0,  0.0,  0.0,  1.32, 0.0],
                             [0.0,  1.0,  0.0,  0.0,  0.0, 0.0],
                             [0.0,  0.0,  1.0,  0.0,  0.0, 0.0],
                             [0.0,  0.0,  0.0,  0.5,  0.0, 0.0],
                             [0.0,  0.0,  0.0,  0.0,  0.5, 0.0],
                             [0.0,  0.0,  0.0,  0.0,  0.0, 1.0]]) 

        self.J6o = np.array([[1.0,  0.22, 0.0,  0.0,  1.34, 0.0],
                             [0.0,  1.0,  0.0,  0.0,  0.22, 0.0],
                             [0.0,  0.0,  1.0,  0.22, 0.0,  0.0],
                             [0.0,  0.0,  0.0,  0.5,  0.0,  0.0],
                             [0.0,  0.0,  0.0,  0.0,  0.5,  0.0],
                             [0.0,  0.0,  0.0,  0.0,  0.0,  1.0]]) 
        
        if discardMixing:
            self.J4  = np.diag([1.,1.,1.,0.5])
            self.J4g = np.diag([1.,1.,1.,0.5])
            self.J6  = np.diag([1.,1.,1.,0.5,0.5,np.nan])
            self.J6o = np.diag([1.,1.,1.,0.5,0.5,np.nan])
        #print "discardMixing", discardMixing

        self.T6 = np.array([[ 1./6,         1./6,      1./6,         1./6,         1./6,      1./6],
                            [-4.5/12,       0,         4.5/12,     -4.5/12,       0,          4.5/12],
                            [ 4.5/46.2,     4.5/46.2,  4.5/46.2,    -4.5/46.2,    -4.5/46.2, -4.5/46.2],
                            [-4.5**2/92.4,  0.,        4.5**2/92.4,  4.5**2/92.4,  0.,       -4.5**2/92.4],
                            [ 9./8,        -18./8,     9./8,         9./8,        -18./8,     9./8],
                            [-0.6575,       1.31494,  -0.6575,       0.6575,      -1.31494,   0.6575]])

        self.T4 = np.array([[0.5,          0.,           0.5,          0.],
                            [-3./4,        3./4,        -3./4,         3./4],
                            [4.5/30.8,     4.5/30.8,    -4.5/30.8,    -4.5/30.8],
                            [-4.5**2/46.2, 4.5**2/46.2,  4.5**2/46.2, -4.5**2/46.2]])

        self.T4_37_39 = np.array([[0.5,          0,            0,            0.5],
                                  [-3./4,        3./4,        -3./4,         3./4],
                                  [4.5/30.8,     4.5/30.8,    -4.5/30.8,    -4.5/30.8],
                                  [-4.5**2/46.2, 4.5**2/46.2,  4.5**2/46.2, -4.5**2/46.2]])

        self.T4_41 = np.array([[0.0, 0.5, 0, 0.5],
                               [3./4, -3./4, -3./4, 3./4],
                               [4.5/30.8,     4.5/30.8,    -4.5/30.8,    -4.5/30.8],
                               [4.5**2/46.2, -4.5**2/46.2,  4.5**2/46.2, -4.5**2/46.2]])

        self.freq2Mp = np.zeros([72*6, self.n_probes])
        #self.mpStation = np.zeros([self.n_probes])
        #i = 0
        for station_n, station in enumerate(self.getStations()):
            #mp = 0
            probes = np.argwhere(self.select(yokes=[station[0]], aziIds=[station[1]]))
            for probe_, probe in enumerate(probes):
                if self.id['n'][probe] == 4:
                     for mp in range(self.id['n'][probe]):
                        if station[0] == 'G':
                            if  station_n in [37, 39]:
                                M = self.J4g.dot(self.T4_37_39)
                            elif station_n in [41]:
                                M = self.J4g.dot(self.T4_41)
                            else:
                                M = self.J4g.dot(self.T4)
                        else:
                            M = self.J4.dot(self.T4)
                        self.freq2Mp[station_n*6 + mp,probe] = M[mp, probe_]
                        #self.mpStation[i+mp] = station_n
                     for mp in range(4,6):
                        self.freq2Mp[station_n*6 + mp,0] = np.nan
                if self.id['n'][probe] == 6:
                    for mp in range(self.id['n'][probe]):
                        if station[0] == 'A':
                           M = self.J6o.dot(self.T6)
                        else:
                            M = self.J6.dot(self.T6)
                        self.freq2Mp[station_n*6 + mp,probe] = M[mp, probe_]
                        #self.mpStation[i+mp] = station_n
            #i = i + probes.shape[0]
            
    def getDQCFlags(self):
        self.activateBranches(["Quality"])
        def callbackqtag():
            return [self.getQuality()]
        qtag = self.loop(callbackqtag)
        self.qtag = qtag[0]
        return self.qtag

    def loadMp(self, discardMixing=False, dqc=0):
        """

        Args:
            dqc: apply dqc mask
        """
        #if not hasattr(self, 'freq2Mp'):
        self.loadJs(discardMixing=discardMixing)
        def transform(event):
            return self.freq2Mp.dot(event)
        if dqc != 0:
            if self.qtag is None:
                self.getDQCFlags()
            freq_ = np.array([util.interp1d(self.time[(self.qtag[:,probe].astype('uint32')&dqc) == 0, probe], self.freq[(self.qtag[:,probe].astype('uint32')&dqc) == 0,probe], fill_value="extrapolate")(self.time[:,probe]) for probe in range(self.n_probes)]).T
        else:
            freq_ = self.freq
        mp = np.apply_along_axis(transform, 1, freq_)
        self.mp = mp.reshape([-1,72,6])
        #for station in ff.fp.getStations():

    def getStationWeight(self, rel=False, only6=False):
        phis = self.getStationPhi() 
        if only6:
           sn = self.getStationN()
           phis = phis[sn==6]
        d = (np.roll(phis,-1) - np.roll(phis,1))
        w = ((d + 360.)%360.)/2.
        if rel:
            w = w/w.sum()
        if only6:
            w_ = np.zeros([72])
            i = 0
            for jj,sn_ in enumerate(sn):
                #print jj, i, sn_, w.shape
                if sn_ == 4:
                    w_[jj] = np.nan
                else:
                    w_[jj] = w[i]
                    i = i + 1
            w = w_
        return w

    def getAziMp(self, n6weights=True, discardMixing=False, dqc=0):
        #if not hasattr(self, 'mp'):
        self.loadMp(discardMixing=discardMixing, dqc=dqc)
        w = self.getStationWeight(rel=True)
        data = [np.nansum(self.mp[:,:,mp]*w, axis=1) for mp in range(4)]
        if n6weights:
            w = self.getStationWeight(rel=True, only6=True)
        data.append(np.nansum(self.mp[:,:,4]*w, axis=1))
        self.aziMp = np.array(data).T
        return self.aziMp

    def loadBasicMode(self, mode_freq=0):
        self.loadPhi()
        self.time, self.freq = self.getBasics(mode_freq=mode_freq)
        self.freqAtTime = []
        for probe in range(self.n_probes):
            self.freqAtTime.append(util.interp1d(self.time[:, probe], self.freq[:, probe]))
    
    def loadPhi(self, event=1):
        if(self.getEntries() >= event):
            self.load(event)
            self.phi = self.getPhi()

    def getStationN(self):
        n = np.zeros([72])
        for i, station in enumerate(self.getStations()):
            n[i] = self.id['n'][self.select(yokes=[station[0]],aziIds=[station[1]])][0]
        return n
 

    def getStationPhi(self, event=1):
        self.loadPhi(event)
        station_phi = []
        for station in self.getStations():
            s = self.select(yokes=[station[0]], aziIds=[station[1]])
            station_phi.append(self.phi[s].mean())
        return np.array(station_phi)

    ## selection in basic mode
    def t(ids=[], yokes=[], radIds=[], aziIds=[], layers=[]):
        s = self.select(ids=[], yokes=[], radIds=[], aziIds=[], layers=[])
        return self.time[s]

    def f(ids=[], yokes=[], radIds=[], aziIds=[], layers=[]):
        s = self.select(ids=[], yokes=[], radIds=[], aziIds=[], layers=[])
        return self.freq[s]

    def tf(ids=[], yokes=[], radIds=[], aziIds=[], layers=[]):
        s = self.select(ids=[], yokes=[], radIds=[], aziIds=[], layers=[])
        return self.time[s], self.freq[s]
        
    def loadSettings(self, DAQ=False):
        """ FP specific settings """
        if DAQ:
            self.fname_path    = "TreeGenFixedProbe/fixedProbe_DAQ"
        else:
            self.fname_path    = "TreeGenFixedProbe/fixedProbe"
        self.n_probes      = 378
        self.yokes  = np.arange(ord('A'), ord('L')+1)
        self.aziIds = np.arange(1,6+1)

    def getStations(self):
        stations = []
        for yoke_ in self.yokes:
            for aziId in self.aziIds:
                stations.append((chr(yoke_), aziId))
        return stations

    def loadIds(self, event=1): 
        #self.activateBranches(["Header"])
        if(self.getEntries() >= event):
            self.load(event)
            self.id = { 'yoke':  self.getYoke(),
                        'azi':   self.getAziId(),
                        'rad':   self.getRadId(),
                        'layer': self.getLayer(),
                        'mux':   self.getMux(),
                        'round': self.getRound(),
                        'phi'  : self.getPhi(),
                        'station' : np.zeros([self.n_probes]),
                        'n' : np.zeros([self.n_probes])
                        };
            for i in range (self.n_probes):
                self.id['station'][i] = (ord(self.id['yoke'][i]) - ord('A'))*6 + self.id['azi'][i] - 1
                self.id['n'][i] = np.argwhere(self.select(yokes=[self.id['yoke'][i]], aziIds=[self.id['azi'][i]])).shape[0]

            self.loadPos()
        else:
            print("Cannot load ids, no data present")

    def loadPos(self):
        from gm2.constants import FP
        self.pos_r     = np.full(self.n_probes, 0.0)
        self.pos_theta = np.full(self.n_probes, 0.0)
        for layer in ['T','B']:
            for rad in ['I','M','O']:
                self.pos_r[     (self.id['layer'] == (layer)) & (self.id['rad']==(rad))] = FP.probes.position.getR(layer, rad)
                self.pos_theta[ (self.id['layer'] == (layer)) & (self.id['rad']==(rad))] = FP.probes.position.getTheta(layer, rad)

    ### Access FP Data from the ROOT file ###
    def getTimeSystem(self):
        return np.frombuffer(self.data.Frequency_SystemTimeStamp, dtype='u8')

    def getTimeGPS(self):
        return np.frombuffer(self.data.Frequency_GpsTimeStamp, dtype='u8')

    def getTriggerDelay(self):
        """Trigger delay (used in mode 3) in 10 us."""
        return np.frombuffer(self.data.Trigger_Delay, dtype='uint16')

    def getTriggerTime(self):
        """Trigger time (per probe)"""
        return np.frombuffer(self.data.Trigger_TimeStamp, dtype='uint32')

    def getTriggerReadoutTime(self):
        return np.frombuffer(self.data.Trigger_ReadoutTime, dtype='uint16')

    def getTriggerBunchInterval(self):
       """"""
       return np.frombuffer(self.data.Trigger_BunchInterval, dtype='uint16')

    def getTriggerSequenceId(self):
       return np.frombuffer(self.data.Trigger_SequenceId, dtype='uint16')

    def getTriggerBunchId(self):
       return np.frombuffer(self.data.Trigger_BunchId, dtype='uint16')

    def getTriggerMode(self):
       return np.frombuffer(self.data.Trigger_Mode, dtype='uint16')

    def getTriggerSource(self):
       return np.frombuffer(self.data.Trigger_Source, dtype='uint16')

    def getQuality(self):
       """Quality Flag. 0 is good.
       
       Bit  0: freq
       Bit  1: amplitude
       Bit  2: length
       Bit  3: power
       Bit  8: field step
       Bit 16: hell-probe
       
       Returns:
           numpy.array(int, [nevents x nprobes]) : quality flag (see above). 0 means healty.
       """
       return  np.frombuffer(self.data.Quality, dtype='u4')

    def getTimeDevice(self):
        return np.frombuffer(self.data.Frequency_DeviceTimeStamp, dtype='u8')

    def getFrequency(self, p=-1):
        if (p < 0)|(p >= 6):
            return np.frombuffer(self.data.Frequency_Frequency, dtype='double').reshape([self.n_probes, -1])
        else:
            return np.frombuffer(self.data.Frequency_Frequency, dtype='double').reshape([self.n_probes, -1])[:,p]

    def getFrequencyUncertainty(self, p=-1):
        if p >= 6:
            raise ValueError('getFrequency() Method p='+str(p)+' >= 6 does not exist.')
        if (p < 0):
            return np.frombuffer(self.data.Frequency_FrequencyUncertainty, dtype='double').reshape([self.n_probes, -1])
        else:
            return np.frombuffer(self.data.Frequency_FrequencyUncertainty, dtype='double').reshape([self.n_probes, -1])[:,p]

    def getAmplitude(self):
        return np.frombuffer(self.data.Signal_Amplitude, dtype='double')

    def getPower(self):
        return np.frombuffer(self.data.Signal_FidPower, dtype='double')

    def getSNR(self):
        return np.frombuffer(self.data.Signal_SNR, dtype='double') 

    def getFidLength(self):
        return np.frombuffer(self.data.Signal_FidLength, dtype='double')

    def getFidChi2(self):
        return np.frombuffer(self.data.Signal_FitChi2, dtype='double')

    def getPhi(self, correct=True):
        data = np.frombuffer(self.data.Position_Phi, dtype='double')
        #if correct:
        #    if data[self.select(yokes=['L'])].mean() < 305.:
        #        data[self.select(yokes=['L'])] += 30.
        return data

    def getX(self):
        return np.frombuffer(self.data.Position_X, dtype='double')

    def getY(self):
        return np.frombuffer(self.data.Position_Y, dtype='double')

    def getAziId(self):
        return np.frombuffer(self.data.Header_AziId, dtype='uint16')

    def getId(self):
        return np.frombuffer(self.data.Header_ProbeId, dtype='a1')

    def getMux(self):
        return np.frombuffer(self.data.Header_MuxId, dtype='uint16')

    def getRound(self):
        return np.frombuffer(self.data.Header_RoundId, dtype='uint16')

    def getLayer(self):
        return np.array([chr(c) for c in np.fromstring(self.data.Header_LayerId, dtype='B')])
        #return np.frombuffer(self.data.Header_LayerId, dtype='a1')

    def getYoke(self): 
        return np.array([chr(c) for c in np.fromstring(self.data.Header_YokeId, dtype='B')])
        #return np.frombuffer(self.data.Header_YokeId, dtype='a1')

    def getRadId(self):
        return np.array([chr(c) for c in np.fromstring(self.data.Header_RadId,dtype='B')])
        #return np.frombuffer(self.data.Header_RadId, dtype='a1')

    def getAlignment(self, n=10):
        self.load(n)
        return [self.getId(), self.getPhi(), self.getYoke(), self.getAziId(), self.getLayer(), self.getRadId()]

    def getHealt(self):
        return np.frombuffer(self.data.Header_Health, dtype='uint16')

    def loop(self, func, ids=[], yokes=[], radIds=[], aziIds=[], layers=[], *args):
        """ loop function with fp specific selections """
        self.loadIds()
        self.getEntry(0)
        sel = self.select(ids=ids, yokes=yokes, radIds=radIds, aziIds=aziIds, layers=layers)
        return self.theLoop(sel, func, *args)
        
    def select(self, ids=[], yokes=[], radIds=[], aziIds=[], layers=[]):
        """ constructs selection, try to run this only once

            Parameters: 
            ids:    0 to 377
            yokes:  'A' to 'L'
            radIds: 'I', 'M', 'O'
            aziIds: 1 to 6
            layers: 'T','B'
        """
        sel = np.full(self.n_probes, True)
        if ids != []:
            sel = sel & np.isin(self.getId(), ids)
        if yokes != []:
            sel = sel & np.isin(self.getYoke(), yokes)
        if radIds != []:
            sel = sel & np.isin(self.getRadId(), radIds)
        if aziIds != []:
            sel = sel & np.isin(self.getAziId(), aziIds)
        if layers != []:
            sel = sel & np.isin(self.getLayer(), layers)
        return sel


    def getBasics(self, time_lims=[], ids=[], yokes=[], radIds=[], aziIds=[], layers=[], mode_freq=0):
        self.activateBranches(["Frequency", "Header"])
        def callback():
            return [self.getTimeGPS(), self.getFrequency(mode_freq)] 
        time, freq = self.loop(callback, ids=ids, yokes=yokes, radIds=radIds, aziIds=aziIds, layers=layers)
        sel = np.full(time.shape[0], True)
        if time_lims != []:
            if len(time_lims) == 2:
                sel = sel & (time.max(axis=1) > time_lims[0]) & (time.min(axis=1) < time_lims[1]) 
            else:
                raise ValueError('getBasics() time_lims needs to be of length 2 ('+str(len(time_lim))+'): phi_lims=[min, max]')
        return time[sel], freq[sel]

