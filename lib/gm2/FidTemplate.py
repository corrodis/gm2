import ROOT
import numpy as np

class FidTemplate:
    """ Class with utilities to access fid templates used in the production."""
    def __init__(self, fname="FixedProbePhaseTemplate.root"):
        """ 
        Args:
            fname (str, optional) : template root file. Defaults to 'FixedProbePhaseTemplate.root'.

        Members:
            fname (str) : used template filename.
            f (ROOT.TFile) : Template Root file
        """
        self.fname = fname
        self.f = ROOT.TFile(fname, "READ")
        self.n_probes = 378

    def getPhase(self, probe, full=False):
        """ Get Phase Template of 'probe'.
        
        Args:
            probe (int) : probe number.
            fill (bool, optional) : if True return phase template of full fids. Defaults to False.

        Returns (numpy.array(float, [fid_length])) : phase temlate.
        """ 
        if full:
            data = self.f.Get("PhaseTemplate")
        else:
            data = self.f.Get("PhaseTemplate")
        return np.array(data).reshape([self.n_probes, -1])[probe, :]

    def getFreq(self, probe = None):
        """ Get frequencie(s) corresponding to the templates.
        
        Args:
            probe (int, optional) : probe index. If None all frequencies are returned. Defaults to None.

        Returns:
            float : Frequency corresponding to phase template of 'probe' if probe is provided.
            np.array(float, [n_probes]) : Frequencies of all probes. If probe = None.
        """
        data = np.array(self.f.Get("FreqTemplate"))
        if probe is None:
            return data
        else:
            return data[probe]
