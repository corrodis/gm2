# Python implementation of of Jason Bono's Special Points

import numpy as np
from scipy.optimize import fminbound
from scipy.optimize import curve_fit
import gm2

class SpecialPoints(object):
    """ Python implementation of Jason Bono's Special Point Method 
    """

    def __init__(self, wf_y = None, wf_x = None, linear_envelope=False, quadratic_envelope=False, frequency_tracking=True):
        """
        
        Args:
             wf_y (ndarray(float, [n], optional) : array with waveform (fid) y values with length n.
             linear_envelope (bool, optional) :  Defaults False.
             quadratic_envelope (bool, optional) : Defaults False.
             frequency_tracking (bool, optional) : Defaults True.


        Examples:
            Use a trolley Waveform
            >>> import gm2
            >>> wf = gm2.TrolleyWf([5417], prefix="FieldPlainRootOutput_")
            >>> data = wf.getFid(100,0)
            >>>
            >>> sp = gm2.SpecialPoints(data[600:10000])
            >>> sp.plot()
            >>> sp.plotFreq() 
        """
        self.linear_envelope_on    = linear_envelope
        self.quadratic_envelope_on = quadratic_envelope
        self.frequency_tracking_on = frequency_tracking
        if not wf_y is None:
            if wf_x is None:
                # assume dt = 1e-6
                wf_x = gm2.np.arange(wf_y.shape[0])*1e-6

            self.load(wf_x, wf_y)
    def load(self, wf_x, wf_y):
        """Load special points

        Args:
             wf_x (ndarray(float, [n])) : array with waveform (fid) x values with length n.
             wf_y (ndarray(float, [n])) : array with waveform (fid) amplitude with length n.
        
        Members:
             up_zeros
             down_zeros
             peaks
             troughs
             peaks_y 
             troughs_y

        """
        self.wf_x = wf_x
        self.wf_y = wf_y

        heightThreshold = 0;

        self.up_zeros   = [] # x-coordinate of up   zeros
        self.down_zeros = [] # x-coordinate of down zeros
        self.peaks      = []
        self.troughs    = []
        self.peaks_y    = []
        self.troughs_y  = []
        self.number_of_good_zeros = 0
        counters = []
        good_line_starts = []
        good_line_ends   = []
        up_zero_y_vals   = [] # y position of up   zeros: always 0
        down_zero_y_vals = [] # y position of down zeros: always 0

        last_zero_was_good = False
        above_threshold = False
        last_zero_x = 0.0

        nConsecutiveZeros = 0
        highestPoint = [0., 0.]
        lowestPoint  = [0., 0.] 

        n = wf_x.shape[0]
        for i in range(1,n-1):
            x      = wf_x[i]
            y      = wf_y[i]
            x_last = wf_x[i-1]
            y_last = wf_y[i-1]
            x_next = wf_x[i+1]
            y_next = wf_y[i+1]
            y_sign = y > 0
            y_last_sign = y_last > 0
            y_next_sign = y_next > 0
            if y > highestPoint[1]:
                highestPoint = [x,y]
            if y < lowestPoint[1]:
                lowestPoint = [x, y]
            
            if y_last_sign != y_sign:
                # ensure that the last highest and lowest points passed threshold
                # bove_threshold = (highestPoint[1]>heightThreshold) && (lowestPoint[1]<-heightThreshold);
                above_threshold = True # HACKED... FIX ME!

                # see if the time ordering of last high and low are correct
                # xor (^) returns true if y is positive and highest occured before lowest
                # or true if y is negative and lowest occured before highest
                # and it returns false otherwise
                in_right_order = (y_sign) ^ (lowestPoint[0]<highestPoint[0])
                in_right_order = True; # HACKED... FIX ME!


                if (in_right_order & above_threshold):
                    # increment the number of consecutive zeros
                    # this is used to ensure the peaks can be fit propery
                    nConsecutiveZeros = nConsecutiveZeros + 1

                    # fill the counter for later plotting of # zeros/peaks  vs time
                    self.number_of_good_zeros = self.number_of_good_zeros + 1;
                    counters.append(self.number_of_good_zeros);

                    this_zero_x = self.InterpolateZero(x_last, y_last, x, y)

                    if y_sign:
                        self.up_zeros.append(this_zero_x)
                        up_zero_y_vals.append(0)
                    else:
                        self.down_zeros.append(this_zero_x)
                        down_zero_y_vals.append(0)
                    
                    # interpolate and fill the peaks
                    # do so only if there have been three or more consecutive zeros
                    if nConsecutiveZeros > 2:
                        improvedPeak = self.InterpolateMinOrMax(wf_x, wf_y, last_zero_x, x, (y_sign)==False, highestPoint[1])

                        if y_sign == False:
                            self.peaks.append(improvedPeak[0])
                            self.peaks_y.append(improvedPeak[1])
                        else:
                            self.troughs.append(improvedPeak[0])
                            self.troughs_y.append(improvedPeak[1])

                    # if the last zero was bad, then this is the beginning of the a string of good zeros - record it!
                    if not last_zero_was_good:
                        good_line_starts.append(x)
                    # Before leaving the loop, record this iterations info for the next one
                    last_zero_was_good = True;
                    last_zero_x = this_zero_x
                else:
                    # you get here if a zero fails the filter
                    # if the last zero was good, then this is the end of the string of good zeros - record it!
                    if last_zero_was_good:
                        good_line_ends.append(x)
                    last_zero_was_good = False;
                    nConsecutiveZeros = 0

                if y_sign:
                    highestPoint = [0., 0.]
                else:
                    lowestPoint  = [0., 0.]

    def plot(self):
        """ Plot the last loaded values"""


    def InterpolateZero(self, x1, y1, x2, y2):
        dx = x2 - x1
        dy = y2 - y1
        return x1 + dx * (0-y1)/dy 

    def InterpolateMinOrMax(self, wf_x, wf_y, x1, x2, is_a_peak, amp):
        linear_envelope_on = self.linear_envelope_on
        quadratic_envelope_on = self.quadratic_envelope_on
        percentOfPoints = 50.0
        percentRemoval = 100. - percentOfPoints
        reducedInterval = percentRemoval*(x2-x1)/(2.0*100.0)
        new_x1 = x1 + reducedInterval
        new_x2 = x2 - reducedInterval

        def sine(x, a, b, c, d):
            return a + b * np.sin(c * x + d)
        
        #p0 = [0, amp, 334000, (x1+x2)/2.]
        p0 = [0, amp, 334000, 0]
        s = (wf_x>new_x1)&(wf_x<new_x2)
        try:
            popt, pcov = curve_fit(sine, wf_x[s], wf_y[s], p0=p0)
            if is_a_peak:
                # max
                max_x = fminbound(lambda x: -sine(x,*popt), new_x1, new_x2, xtol=1e-09)
            else:
                # min
                max_x = fminbound(lambda x: sine(x,*popt), new_x1, new_x2, xtol=1e-09)
       
            stationary_point   = max_x
            stationary_y_point = sine(max_x, *popt)
        except:
            #print("Fit failed")
            popt = p0
            stationary_point = (x1 + x2)/2.
            stationary_y_point = sine(stationary_point, *popt)
            return [np.nan, np.nan] 

        '''import matplotlib.pyplot as plt
        plt.plot( wf_x[s], wf_y[s],'.')
        plt.plot( wf_x[s], sine(wf_x[s], *popt),'-')
        plt.plot(stationary_point,stationary_y_point,'o')
        plt.show()'''
        

        #return [np.nan, np.nan] 
        # expand the fitting range for linear and quadratic envelopes
        if linear_envelope_on:
            percentOfPoints = 80.0;
        if quadratic_envelope_on:
            percentOfPoints = 100.0;

        percentRemoval = 100. - percentOfPoints;
        reducedInterval = percentRemoval*(x2-x1)/(2.0*100.0);
        new_x1 = x1 + reducedInterval;
        new_x2 = x2 - reducedInterval;

        def linSine(x, a, b, c, d, e ,f):
            return a + b * np.sin(c * x + d) * (1.0 + e * (x - f))

        def quadSine(x, a, b, c, d, e ,f, g, h):
            return a + b * np.sin(c * x + d) * (1.0 + e * (x - f) + g * (x-h)**2)

        if linear_envelope_on | quadratic_envelope_on:
            p0 = popt + [0., new_x1]
            s = (wf_x>new_x1)&(wf_x<new_x2)
            popt, pcov = curve_fit(linSine, wf_x[s], wf_y[s], p0=p0)
            if is_a_peak:
                max_x = fminbound(lambda x: -sine(x,*popt), new_x1, new_x2)
            else:
                max_x = fminbound(lambda x: sine(x,*popt), new_x1, new_x2)

            stationary_point   = max_x
            stationary_y_point = sine(max_x, *popt)
            #stationary_point   = max_x[0]
            #stationary_y_point = max_x[1]

        if quadratic_envelope_on:
            p0 = popt + [0, new_x1]
            p0[5] = new_x1
            s = (wf_x>new_x1)&(wf_x<new_x2)
            popt, pcov = curve_fit(linSine, wf_x[s], wf_y[s], p0=p0)
            if is_a_peak:
                max_x = fminbound(lambda x: -sine(x,*popt), new_x1, new_x2)
            else:
                max_x = fminbound(lambda x: sine(x,*popt), new_x1, new_x2)

            stationary_point   = max_x
            stationary_y_point = sine(max_x, *popt)
            #stationary_point   = max_x[0]
            #stationary_y_point = max_x[1]

        return [stationary_point, stationary_y_point]


    def plot(self, show=True):
         import matplotlib.pyplot as plt
         import seaborn as sns
         plt.plot(self.wf_x, self.wf_y, '-', linewidth=1)
         plt.plot(self.up_zeros,   np.zeros_like(self.up_zeros), '.', label="Up Zero Crossings")
         plt.plot(self.down_zeros, np.zeros_like(self.down_zeros),    '.', label="Down Zero Crossings")
         plt.plot(self.peaks,      self.peaks_y,                 '.', label="Max Crossings")
         plt.plot(self.troughs,    self.troughs_y,               '.', label="Min Crossings")
         plt.legend()
         plt.xlabel("time [s]")
         plt.ylabel("amplitude [a.u.]")
         #sns.despine()
         #plt.tight_layout()
         gm2.despine()
         if show:
             plt.show()

    def plotFreq(self, show=True, fid=True, ylim=[]):
        from gm2 import plt, np
        ax1 = plt.subplot(111)
        ax1.plot(gm2.np.array(self.up_zeros)*1e3, 1.0/np.gradient(self.up_zeros)/1e3, '.', markersize=2)
        ax1.plot(gm2.np.array(self.down_zeros)*1e3, 1.0/np.gradient(self.down_zeros)/1e3, '.', markersize=2)
        ax1.plot(gm2.np.array(self.peaks)*1e3, 1.0/np.gradient(self.peaks)/1e3, '.', markersize=2)
        ax1.plot(gm2.np.array(self.troughs)*1e3, 1.0/np.gradient(self.troughs)/1e3, '.', markersize=2)
        ax1.set_ylabel("frequency [kHz]")
        #ax1.set_ylim(ylim)
        if fid:
            ax2 = ax1.twinx()
            ax2.plot(self.wf_x*1e3, self.wf_y/self.wf_y.max(), '-', markersize=2, alpha=0.3, color=gm2.sns.color_palette()[8])
        if show:
            gm2.despine()
            plt.show()
