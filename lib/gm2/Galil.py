# package gm2
# module galil
# Author: Simon Corrodi
#         scorrodi@anl.gov
#         September 2018

import numpy as np
from gm2 import rootbase, util

#import matplotlib.pyplot as plt
#from scipy.interpolate import interp1d
#from gm2util import *

class Galil(rootbase, object):
    def __init__(self, runs = [], prefix=None):
        self.runs = runs
        self.loadSettings()
        super(Galil, self).__init__('galil', prefix=prefix)
        self.loadFiles()
        self.EncToDeg = 360.0/433221.0;
        
    def encToDeg(self,pos):
        pos_ = (pos[:,0] - pos[:,1])/2.0*self.EncToDeg + 175.0 # bugfix because pos[:,2] is corrupted
        pos_ = (pos_ + 360.)% 360.
        return pos_

    def loadSettings(self):
        """ Trolley specific settings """
        self.fname_path     = "TreeGenGalilTrolley/tGalil"
        self.n_probes = 1

    ### Access Galil Data from the ROOT file ###
    def getTime(self):
        return self.data.Trolley_TimeStamp

    def getTension(self, m = -1):
        data =  np.frombuffer(self.data.Trolley_Tensions, dtype='double').reshape([2])
        if m < 0:
            return data
        else:
            return data[m]

    def getTemperature(self, m = -1):
        data =  np.frombuffer(self.data.Trolley_Temperatures, dtype='double').reshape([2])
        if m < 0:
            return data
        else:
            return data[m]

    def getControleZVoltage(self, m):
        data =  np.frombuffer(self.data.Trolley_ControleVoltage, dtype='double').reshape([3])
        if m < 0:
            return data
        else:
            return data[m]

    def getPosition(self, m=-1):
        """
        
        Returns:
            ndarray(float, [3]) : encoder, encoder, online odb position
        """
        data =  np.frombuffer(self.data.Trolley_Positions, dtype='double').reshape([3])
        if m < 0:
            return data
        else:
            return data[m]

    def getVelocity(self, m):
        data =  np.frombuffer(self.data.Trolley_Velocities, dtype='double').reshape([3])
        if m < 0:
            return data
        else:
            return data[m]

    def loop(self, func, *args):
        """ loop function with trolley specific selections """
        self.getEntry(0)
        return self.theLoop([], func, *args)

    def loadTension(self):
        """Loads tension and provides it as a function of position """
        #def callback():
        #    return [self.getPosition(2), self.getTension()] 
        def callback():
            return [self.getPosition(), self.getTension()]
        pos_, self.tension = self.loop(callback)
        EncToDeg = 360.0/433221.0;
        self.pos = (pos_[:,0] - pos_[:,1])/2.0*EncToDeg + 175.0 # bugfix because pos[:,2] is corrupted
        self.pos = (self.pos + 360.)% 360.
        self.tensionAt = [util.interp1d(self.pos[:], self.tension[:,0], fill_value='extrapolate'),
                          util.interp1d(self.pos[:], self.tension[:,1], fill_value='extrapolate')]
 

    def plotTension(self, cable=-1, show=True):
        import gm2
        def callback():
            return [self.getPosition(), self.getTension()]
        pos, ten = self.loop(callback)
        pos_ = self.encToDeg(pos)
        if cable in [-1,0]:
            gm2.plt.plot(pos_[1:], ten[1:,0], '.', markersize=2, label="signal run %i" % self.runs[0])
        if cable in [-1,1]:
             gm2.plt.plot(pos_[1:], ten[1:,1], '.', markersize=2, label="fish run %i" % self.runs[0])
        if show:
            gm2.plt.ylabel("tension [kg]")
            gm2.plt.xlabel("azimuth [deg]")
            gm2.plt.legend(markerscale=4)
            gm2.despine()
            gm2.plt.show()
