import Fid
import gm2

class FidViewer(object):
    """Class which bundles different fid tools
    
    
    Examples:
        Trolley Data
        >>> template_file = "/home/scorrodi/Gm2FieldOffline/art/OfflineDev_v9_21_01/srcs/gm2field/g2RunTimeFiles/TrolleyProbePhaseTemplate.root"
        >>>
        >>> wf = gm2.TrolleyWf([5415], prefix="FieldPlainRootOutput_")
        >>> data = wf.getFid(100,0)
        >>> 
        >>> view = gm2.FidViewer(data=data, trolley=True, template=template_file, probe=0)
        >>> view.TrolleyCorrection()

        Look at Fids
        >>> wf = gm2.TrolleyWf([5415], prefix="FieldPlainRootOutput_")
        >>> data = wf.getFid(100,0)
        >>> view = gm2.FidViewer(data)
        >>>
        >>> view.plotFid()
        >>> view.plotPhase()
        >>> view.plotFreq()

    """
    def __init__(self, data, trolley=False, fxp=False, pp=False, template="", probe=0, run=True, noFilter=False):
        """
        Args:
             data (ndarray(float, [n])) : fid data
             trolley (bool, optional) : If True trolley settings are loaded. Defaults to False.
             template (string, optional) : template file name. Defaults to "". 
             probe (int, optional) : specifies which probes settings are used. Defaults to 0.
             run (bool, optional) : if True the fids code is executed. Defaults to True.

        Members:
            fid : Fid class (gm2fieldsignal)
            sp : SpecialPoints class (Jason)
        """
        self.TruncateBeginning = 0
        self.TruncateEnd = -1
        self.fid = Fid.Fid()
        self.fid.SetPhaseFitScheme(0)
        self.trolley = False
        self.t0_shift = 0.0
        self.probe = probe
        self.raw_data = data
        if trolley:
            self.loadTrolleySettings()
        elif fxp:
            self.loadFxpSettings()
        elif pp:
            self.loadPPSettings()
            self.setWf(data, probe, dt=1e-7)
        if not pp: 
            self.setWf(data, probe)
        if not template == "":
            self.loadTemplate(template, probe, fxp=fxp)
        self.done = False
        self.sp = None
        if noFilter:
            self.disableFilter()
            self.fid.SetBaselineOption(0)
        if run:
            self.run()
            self.done = True
            self.specialPoints()

    def disableFilter(self):
        print("Reset Filter ")
        self.fid.SetParameter("filter_low_freq",0)
        self.fid.SetParameter("filter_high_freq",1e15)
        self.fid.SetParameter("filter_freq_width",1e15)

    def setT0(self, t0):
        """Sets new t0_shift and reloads the data accordingly"""
        self.t0_shift = t0
        self.setWf(self.raw_data, self.probe)

    def loadFxpSettings(self):
        filter_low_freq      = 0.0
        filter_high_freq     = 20000.0
        window_filter_type   = False
        const_baseline_start = 0
        const_baseline_end   = 400

        self.TruncateBeginning = 0
        self.TruncateEnd       = 4095

        self.t0_shift = -4.4e-4
        edge_width           = 2e-5
        edge_ignore          = 6e-5
        start_amplitude      = 0.37
        baseline_freq_thresh = 500.0
        filter_low_freq      = 0.0
        filter_high_freq     = 200000.0
        filter_freq_width    = 100000.0
        fft_peak_width       = 20000.0
        centroid_thresh      = 0.01
        hyst_thresh          = 0.7
        snr_thresh           = 10.0
        len_thresh           = 0.25
        phasePol             = 1
        window_filter_type   = False

        self.fid.SetParameter("edge_width",edge_width)
        self.fid.SetParameter("edge_ignore",edge_ignore)
        self.fid.SetParameter("start_amplitude",start_amplitude)
        self.fid.SetParameter("baseline_freq_thresh",baseline_freq_thresh)
        self.fid.SetParameter("filter_low_freq",filter_low_freq)
        self.fid.SetParameter("filter_high_freq",filter_high_freq)
        self.fid.SetParameter("filter_freq_width",filter_freq_width)
        self.fid.SetParameter("fft_peak_width",fft_peak_width)
        self.fid.SetParameter("centroid_thresh",centroid_thresh)
        self.fid.SetParameter("hyst_thresh",hyst_thresh)
        self.fid.SetParameter("snr_thresh",snr_thresh)
        self.fid.SetParameter("len_thresh",len_thresh)
        self.fid.SetPoln(phasePol)
        self.fid.SetWindowFilterType(window_filter_type)

        self.fid.SetBaselineOption(2)
        self.fid.SetConstBaselineLimits(const_baseline_start, const_baseline_end)

    def loadFxpSettings(self):
        filter_low_freq      = 0.0
        filter_high_freq     = 20000.0
        window_filter_type   = False
        const_baseline_start = 0
        const_baseline_end   = 400

        self.TruncateBeginning = 0
        self.TruncateEnd       = 4095

        self.t0_shift = -4.4e-4
        edge_width           = 2e-5
        edge_ignore          = 6e-5
        start_amplitude      = 0.37
        baseline_freq_thresh = 500.0
        filter_low_freq      = 0.0
        filter_high_freq     = 200000.0
        filter_freq_width    = 100000.0
        fft_peak_width       = 20000.0
        centroid_thresh      = 0.01
        hyst_thresh          = 0.7
        snr_thresh           = 10.0
        len_thresh           = 0.25
        phasePol             = 1
        window_filter_type   = False

        self.fid.SetParameter("edge_width",edge_width)
        self.fid.SetParameter("edge_ignore",edge_ignore)
        self.fid.SetParameter("start_amplitude",start_amplitude)
        self.fid.SetParameter("baseline_freq_thresh",baseline_freq_thresh)
        self.fid.SetParameter("filter_low_freq",filter_low_freq)
        self.fid.SetParameter("filter_high_freq",filter_high_freq)
        self.fid.SetParameter("filter_freq_width",filter_freq_width)
        self.fid.SetParameter("fft_peak_width",fft_peak_width)
        self.fid.SetParameter("centroid_thresh",centroid_thresh)
        self.fid.SetParameter("hyst_thresh",hyst_thresh)
        self.fid.SetParameter("snr_thresh",snr_thresh)
        self.fid.SetParameter("len_thresh",len_thresh)
        self.fid.SetPoln(phasePol)
        self.fid.SetWindowFilterType(window_filter_type)

        self.fid.SetBaselineOption(2)
        self.fid.SetConstBaselineLimits(const_baseline_start, const_baseline_end)

    def loadTrolleySettings(self):
        """Load Tier1 style trolley settings for fids"""
        self.trolley = True
        
        self.TruncateBeginning = 350
        self.TruncateEnd       = 12600

        self.t0_shift = -3.0e-4
        edge_width           = 2e-5
        edge_ignore          = 4e-5
        start_amplitude      = 0.37
        baseline_freq_thresh = 500.0
        filter_low_freq      = 10.0
        filter_high_freq     = 90000.0
        filter_freq_width    = 60000.0
        fft_peak_width       = 20000.0
        centroid_thresh      = 0.01
        hyst_thresh          = 0.7
        snr_thresh           = 10.0
        len_thresh           = 0.5
        phasePol             = 1
        window_filter_type   = False

        self.fid.SetParameter("edge_width",edge_width)
        self.fid.SetParameter("edge_ignore",edge_ignore)
        self.fid.SetParameter("start_amplitude",start_amplitude)
        self.fid.SetParameter("baseline_freq_thresh",baseline_freq_thresh)
        self.fid.SetParameter("filter_low_freq",filter_low_freq)
        self.fid.SetParameter("filter_high_freq",filter_high_freq)
        self.fid.SetParameter("filter_freq_width",filter_freq_width)
        self.fid.SetParameter("fft_peak_width",fft_peak_width)
        self.fid.SetParameter("centroid_thresh",centroid_thresh)
        self.fid.SetParameter("hyst_thresh",hyst_thresh)
        self.fid.SetParameter("snr_thresh",snr_thresh)
        self.fid.SetParameter("len_thresh",len_thresh)
        self.fid.SetPoln(phasePol)
        self.fid.SetWindowFilterType(window_filter_type)

    def loadTemplate(self, template, probe, fxp=False):
        """Load templates from root files (tier1 style)"""
        f = gm2.ROOT.TFile(template)
        if fxp:
            phase = gm2.np.array(f.Get("PhaseTemplate")).reshape(378,-1)
            self.phase_template = phase[probe,0:-1]
            freq = gm2.np.array(f.Get("FreqTemplate"))
            self.freq_template = gm2.np.array([freq[probe]])
        else:
            n = f.Get("PhaseTemplate_%i" % probe).size()
            self.phase_template = gm2.np.zeros([n])
            for i, v in enumerate(f.Get("PhaseTemplate_%i" % probe)):
                self.phase_template[i] = v
            self.freq_template = gm2.np.array([f.Get("FreqTemplate")[probe]])
        
        self.fid.SetPhaseFitScheme(1)
        self.fid.SetFreqTemplate(self.freq_template)
        self.fid.SetPhaseTemplate(self.phase_template)

    def setWf(self, data, probe, dt=1e-6):
        """Set the fid"""
        self.offset = self.TruncateBeginning * dt
        self.wfLength = data[self.TruncateBeginning:self.TruncateEnd].shape[0]
        self.fid.SetWf(data[self.TruncateBeginning:self.TruncateEnd], dt, 1, t0_shift=self.t0_shift + self.TruncateBeginning * dt)
        
    def run(self):
        """Run fid class."""
        self.fid.Init("Standard")
        self.done = True

    def TrolleyCorrection(self):
        """Tier1 style trolley correction
        
        Returns:
            not implemented yet
        """
        if self.done == False:
            print("You need tun call run() first.")
            return
        IWf = self.fid.i_wf_array()[0]
        FWf = self.fid.f_wf_array()[0]

        if FWf - IWf > 40:
           SmoothResidual = Fid.Smoothen(self.fid.res(), 1, self.wfLength, int(IWf), int(FWf), 20, 3)
           TimeAxis = self.fid.tm()
           gTemp = gm2.ROOT.TGraph(self.wfLength, TimeAxis, SmoothResidual)
           f5 = gm2.ROOT.TF1("f5", "[0]+[1]*x+[2]*pow(x,3)+[3]*pow(x,5)", 0.0, 0.014)
           f5.SetParameters(0, 0, 0, 0)
           TFitStart = TimeAxis[IWf + 1]
           TFitEnd = TimeAxis[IWf + 1] + (TimeAxis[FWf - 1] - TimeAxis[IWf + 1]) / 3.0
           status = gTemp.Fit(f5, "MWEQ", "", TFitStart, TFitEnd)

           TFitEnd = TimeAxis[IWf + 1] + (TimeAxis[FWf - 1] - TimeAxis[IWf + 1]) / 4.0
           status = gTemp.Fit(f5, "MWEQ", "", TFitStart, TFitEnd);

           print self.fid.freq_array()
           print "Parameters:", f5.GetParameter(1), f5.GetParameter(2), f5.GetParameter(3)
           print "Chi2:", f5.GetChisquare(), "/", f5.GetNDF(), "=", f5.GetChisquare()/f5.GetNDF()
           print "Correction: ", f5.GetParameter(1) / 2.0 / gm2.np.pi, "Hz"
           return self.fid.freq_array(), f5.GetParameter(1) / 2.0 / gm2.np.pi


    def specialPoints(self, filtered=True):
        """Apply Jason's special points in the filtered/raw fid
        
        Args:
            filtered (bool, optional) : If True uses filtered waveform, otherwise raw. Defaults to True.
        """
        if filtered: 
            if self.done == False:
                print("You need tun call run() first to use the filtered fid.")
                return
            data = self.fid.filtered_wf()
        else:
            data = sef.fid.wf()
        self.sp = gm2.gm2.SpecialPoints(data, self.fid.tm())


    def plotFidFancy(self, show=True, ):
        fig, ax = gm2.plt.subplots()
        #from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
        ax.plot(self.fid.tm()*1e3, self.fid.wf()/self.fid.wf().max(),'-', markersize=2, label="waveform")
        
        axins = gm2.plt.axes([.60, .7, .3, .195])
        axins.plot(self.fid.tm()*1e3, self.fid.wf()/self.fid.wf().max(),'-', markersize=2, label="waveform")
        axins.set_xlim([0.1,0.2])
        #axins.set_ylim([0.1,0.2.])
        

        import matplotlib.patches as patches
        rect = patches.Rectangle((0.1,-1.2),0.1,2.4,linewidth=1,edgecolor='black',facecolor='none', zorder=20, alpha=0.7)
        ax.add_patch(rect)
        ax.plot([0.2,1.8],[1.2,1.3],'--',color="black", alpha=0.5, linewidth=0.5)
        ax.plot([0.2,3.62],[-1.2,0.57],'--',color="black", alpha=0.5, linewidth=0.5)

        ax.set_xlabel("time [ms]")
        ax.set_ylabel("amplitude [a.u]")
        gm2.despine()
        if show:
            gm2.plt.show()

    def plotFid(self, show=True, onlyRaw=False, fitRange=None, markers=[], markers_label=[], title=""):
        """Plots the raw fids, the filtered, baseline, ... 
        
        Args:
            show (bool, optional) : If true plt.show() is called. Defaults to True.
            onlyRaw (bool, optioal) : plot only the raw fid.
        """
        if self.done == False:
             print("You need tun call run() first.")
             return

        from gm2 import plt, np
        plt.plot(self.fid.tm()*1e3, self.fid.wf(),          '.-', markersize=2, label="waveform")
        if not onlyRaw:
            plt.plot(self.fid.tm()*1e3, self.fid.filtered_wf(), '.-', markersize=2, label="filtered waveform")
            plt.plot(self.fid.tm()*1e3, self.fid.baseline(),    '.-', markersize=2, label="baseline")
            plt.plot(self.fid.tm()*1e3, self.fid.wf_im(),       '.-', markersize=2, label="waveform im")
        ylim = plt.gca().get_ylim()
        if not fitRange:
            if len(fitRange) == 2:
                plt.plot([self.fid.tm()[self.fid.i_wf_array()[0]]*1e3, self.fid.tm()[self.fid.i_wf_array()[0]]*1e3], ylim, '-', color='black')
                plt.plot([self.fid.tm()[self.fid.f_wf_array()[0]]*1e3, self.fid.tm()[self.fid.f_wf_array()[0]]*1e3], ylim, '-', color='black') 
        else:
            plt.plot([self.fid.tm()[fitRange[0]]*1e3, self.fid.tm()[fitRange[0]]*1e3], ylim, '-', color='black')
            plt.plot([self.fid.tm()[fitRange[1]]*1e3, self.fid.tm()[fitRange[1]]*1e3], ylim, '-', color='black')
            
        for mi, marker in enumerate(markers):
            if len(markers_label) == len(markers):
                plt.plot([self.fid.tm()[marker]*1e3, self.fid.tm()[marker]*1e3], [ylim[0], ylim[0]*0.8], '-', color=gm2.sns.color_palette()[mi+1], label=markers_label[mi])
                plt.plot([self.fid.tm()[marker]*1e3, self.fid.tm()[marker]*1e3], [ylim[1], ylim[1]*0.8], '-', color=gm2.sns.color_palette()[mi+1])
            else:
                plt.plot([self.fid.tm()[marker]*1e3, self.fid.tm()[marker]*1e3], [ylim[0], ylim[0]*0.8], '-', color=gm2.sns.color_palette()[mi+1])
                plt.plot([self.fid.tm()[marker]*1e3, self.fid.tm()[marker]*1e3], [ylim[1], ylim[1]*0.8], '-', color=gm2.sns.color_palette()[mi+1])
        if not onlyRaw:
            plt.plot([self.fid.fid_time_array()[0]*1e3, self.fid.fid_time_array()[0]*1e3], ylim, '-', color='black', alpha=0.5)
            plt.plot([self.fid.t0_shift*1e3, self.fid.t0_shift*1e3], ylim, '-', color='black', alpha=0.5)

        plt.title(title)
        plt.ylabel("amplitude [adc]")
        plt.xlabel("time [ms]")
        plt.legend()
        if show:
            gm2.despine()
            plt.show()

    def plotPhase(self, show=True, sp=True):
        """Plots the phase plots for special points and hilber transform.
        
        Args:
            show (bool, optional) : If true plt.show() is called. Defaults to True.
            sp (bool, optiomnal) : If True special points are added. Defaults to True.
        """
        if self.done == False:
             print("You need tun call run() first.")
             self.done()
             return

        from gm2 import plt, np
        plt.plot(self.fid.tm()*1e3, self.fid.phi(), '.', markersize=2, label="Hilbert Transform")
        zero_tm = self.fid.zero_tm()[self.fid.zero_tm()>0]
        zero_phi =  np.arange(zero_tm.shape[0])*np.pi
        plt.plot(zero_tm*1e3, zero_phi, '.', markersize=2, label="zero crossing")

        if sp:
            if not self.sp is None:
                plt.plot(gm2.np.array(self.sp.up_zeros)*1e3, np.arange(len(self.sp.up_zeros))*np.pi*2, '.', markersize=2, label="sp: up")
                plt.plot(gm2.np.array(self.sp.down_zeros)*1e3, np.arange(len(self.sp.down_zeros))*np.pi*2, '.', markersize=2, label="sp: down")
                plt.plot(gm2.np.array(self.sp.peaks)*1e3, np.arange(len(self.sp.peaks))*np.pi*2, '.', markersize=2, label="sp: peaks")
                plt.plot(gm2.np.array(self.sp.troughs)*1e3, np.arange(len(self.sp.troughs))*np.pi*2, '.', markersize=2, label="sp: troughs")

        ylim = plt.gca().get_ylim()
        plt.plot([self.fid.tm()[self.fid.i_wf_array()[0]]*1e3, self.fid.tm()[self.fid.i_wf_array()[0]]*1e3], ylim, '-', color='black')
        plt.plot([self.fid.tm()[self.fid.f_wf_array()[0]]*1e3, self.fid.tm()[self.fid.f_wf_array()[0]]*1e3], ylim, '-', color='black')
        plt.legend()
        plt.xlabel("time [ms]")
        plt.ylabel("phase [rad]")
        if show:
            gm2.despine()
            plt.show()

    def plotRes(self, show=True, n=0, zc=True, sp=True, ylim=None, noLegend=False):
        """Plot the residuals
        
        Args:
            show (bool, optional) : If true plt.show() is called. Defaults to True.
            n (int,optional) : which fid from patch should be shown. Defaults to 0.
        """
        gm2.plt.plot(self.fid.tm()*1e3, self.fid.res(), label="hilbert transform")
        gm2.plt.xlabel("time [ms]")
        gm2.plt.ylabel("phase residual [rad]")
 
        if sp:
            if not self.sp is None:
                from gm2 import np, plt
                fit_para = self.fid.fit_parameters()[0]
                fit_start = self.fid.tm()[self.fid.i_wf_array()[0]]
                fit_end = self.fid.tm()[self.fid.f_wf_array()[0]]
                up_zeros_res_tm = gm2.np.array(self.sp.up_zeros)
                up_zeros_res = np.arange(len(self.sp.up_zeros))*np.pi*2 - (fit_para[0] + fit_para[1] * up_zeros_res_tm)
                up_zeros_res_s = (up_zeros_res_tm >= fit_start)&(up_zeros_res_tm < fit_end)
                plt.plot(gm2.np.array(self.sp.up_zeros)*1e3, up_zeros_res - up_zeros_res[up_zeros_res_s].mean(), '.', markersize=2, label="sp: up")
                down_zeros_res_tm = gm2.np.array(self.sp.down_zeros)
                down_zeros_res = np.arange(len(self.sp.down_zeros))*np.pi*2 - (fit_para[0] + fit_para[1] * down_zeros_res_tm)
                down_zeros_res_s = (down_zeros_res_tm >= fit_start)&(down_zeros_res_tm < fit_end)
                plt.plot(gm2.np.array(self.sp.down_zeros)*1e3, down_zeros_res - down_zeros_res[down_zeros_res_s].mean(), '.', markersize=2, label="sp: down")
                peaks_res_tm = gm2.np.array(self.sp.peaks)
                peaks_res = np.arange(len(self.sp.peaks))*np.pi*2 - (fit_para[0] + fit_para[1] * peaks_res_tm)
                peaks_res_s = (peaks_res_tm >= fit_start)&(peaks_res_tm < fit_end)
                plt.plot(gm2.np.array(self.sp.peaks)*1e3, peaks_res - peaks_res[peaks_res_s].mean(), '.', markersize=2, label="sp: peaks")
                troughs_res_tm = gm2.np.array(self.sp.troughs)
                troughs_res = np.arange(len(self.sp.troughs))*np.pi*2 - (fit_para[0] + fit_para[1] * troughs_res_tm)
                troughs_res_s = (troughs_res_tm >= fit_start)&(troughs_res_tm < fit_end)
                plt.plot(gm2.np.array(self.sp.troughs)*1e3, troughs_res - troughs_res[troughs_res_s].mean(), '.', markersize=2, label="sp: troughs")

        if zc:
            pass
            zero_tm = self.fid.zero_tm()[self.fid.zero_tm()>0].copy()
            zero_phi =  gm2.np.arange(zero_tm.shape[0])*gm2.np.pi
            fit_para = self.fid.fit_parameters()[0]
            zero_res = zero_phi - (fit_para[0] + fit_para[1] * zero_tm)
            gm2.plt.plot(zero_tm*1e3, zero_res, '.', markersize=2, label="zero crossing")

        if show:
            if sp or zc:
                if not noLegend:
                    gm2.plt.legend()
            if ylim:
                plt.ylim([-ylim, ylim])
            gm2.despine()
            gm2.plt.show()

    def plotFreq(self, show=True, sp=True, fid=True, xlim=None, ylim=[45,65], noH=False):
        """Plots the frequency as a function of time as dphi/dt.
        
        Args:
            show (bool, optional) : If true plt.show() is called. Defaults to True.
            sp (bool, optiomnal) : If True special points are added. Defaults to True.
            fid (bool, optional)  : If true the fid is superimposed. Defaults to True.

            ylim ([ymin, ymax], optional) : ylim in kHz. Defaults to [45,65]. 
        """
        if self.done == False:
           print("You need tun call run() first.")
           return

        from gm2 import plt, np
        ax1 = plt.subplot(111)
        zero_tm = self.fid.zero_tm()[self.fid.zero_tm()>0]
        if not noH:
            ax1.plot(self.fid.tm()*1e3, np.gradient(self.fid.phi())/self.fid.dt/2/np.pi/1e3, '.', markersize=2, label="hilbert")
        ax1.plot(zero_tm[0::2]*1e3, 1.0/np.gradient(zero_tm[0::2])/1e3, '.', markersize=2, label="up")
        ax1.plot(zero_tm[1::2]*1e3, 1.0/np.gradient(zero_tm[1::2])/1e3, '.', markersize=2, label="down")

        if sp:
            if not self.sp is None:
                ax1.plot(gm2.np.array(self.sp.up_zeros)*1e3, 1.0/np.gradient(self.sp.up_zeros)/1e3, '.', markersize=2, label="sp: up")
                ax1.plot(gm2.np.array(self.sp.down_zeros)*1e3, 1.0/np.gradient(self.sp.down_zeros)/1e3, '.', markersize=2, label="sp: down")
                ax1.plot(gm2.np.array(self.sp.peaks)*1e3, 1.0/np.gradient(self.sp.peaks)/1e3, '.', markersize=2, label="sp: peak")
                ax1.plot(gm2.np.array(self.sp.troughs)*1e3, 1.0/np.gradient(self.sp.troughs)/1e3, '.', markersize=2, label="sp: troughs")

        ax1.set_ylabel("frequency [kHz]")
        ax1.set_xlabel("time [ms]")
        ax1.set_ylim(ylim)
        if not xlim is None:
            ax1.set_xlim(xlim)
        if fid:
            ax2 = ax1.twinx()
            ax2.plot(self.fid.tm()*1e3, self.fid.filtered_wf()/self.fid.filtered_wf().max(), '-', markersize=2, alpha=0.3, color=gm2.sns.color_palette()[8])
        if show:
            #ax1.legend(markerscale=4)
            gm2.despine()
            plt.show()

