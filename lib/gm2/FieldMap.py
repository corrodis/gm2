import gm2

class FieldMap:
    """Class for Trolley Field Maps.
    
    Attributes:
        azi[sevents] (array(float), optional): azimuth of multipole fits
        m[sevents, N] ( optional): N multipole coefficients 
        res[sevents, 17] ( optional): residuals from multipole fit
        chi2[sevents] ( optional): chi2 from multipole fit
        tr (gm2.Trolley, optional): trolley run data.
        raw_azi[nevents, 17]: the raw azimuths.
        raw_freq[nevents, 17]: the raw frequencies.
        freqAt[probe] (optional): interpolated frequencies from the selected trolley readout cycles (s).
        s[neverns] (bool):  selection of used trolley readout cycles, defaults to all True.
        spk[17] (list(gm2.Spikes), optional): trolley spikes class.
        spk_s[nevents, 17]: selection of good frequencies, per probe.
        fr[17] (list(gm2.Fourier), optional): probe wise fourier fit in phi.
    """
    def __init__(self, runs = None, azi = None, freq = None, load = "", prefix=None, useEncoder=False, mp_n=6):
        """Trolley Field Map Constructor.
        
        Args:
            runs (list[int], optional): run numbers. Specified trolley run is loaded and azi and freq are used.
            azi[neventsx17]  (array, optional): array with raw azimuth. If specified also freq needs to be provided.
            freq[neventsx17]  (array, optional): array with raw frequencies. If specified also azi needs to be provided.
            load (bool): 'simple' load multipoles with uniform weight
                         'cov' load multipoles with resolution and correlations from data
        """
        self.spk_th = gm2.PPM_HZ/2.
        self.runs = runs
        self.prefix = prefix
        self.azi = None
        
        if azi is None:
            if not(runs is None):
                self.tr = gm2.Trolley(runs, False, prefix=prefix)
                if useEncoder: 
                    self.raw_time, self.raw_azi, self.raw_freq = self.tr.getBasics(mode_phi=0)
                else:    
                    self.raw_time, self.raw_azi, self.raw_freq = self.tr.getBasics()
        else:
            if freq is None:
                raise Exception("Azi und freq need to be provided.")
            else:
                if azi.shape != freq.shape:
                    raise Exception("azi %s und freq %s need to have the same dimensions." % (str(azi.shape), str(freq.shape)))
                self.raw_azi  = azi
                self.raw_freq = freq
        self.s = gm2.np.full(self.raw_azi.shape[0], True)
        
        self.spk = None
        self.spk_s = None
        self.fr = None
        self.freqAt = None
        self.oscillationsAt = None
        self.drift = None
        self.d     = None
        if load == 'simple':
            self.loadSpikes()
            self.multipoles(cov = gm2.np.diag(gm2.np.array([17.0]*17)**2), n_=mp_n)
        if load == 'cov':
            self.loadSpikes()
            cov = self.dataCov(nsigma=3.0)
            self.multipoles(cov=cov, n_=mp_n)

    def loadSpikes(self, th = gm2.PPM_HZ*10, skip = None):
        """Loads Trolley Spikes.

        Loads the trolley spikes for all channels as well as probe wise and a global selection for good events.

        Args:
            th (float): spike threshold in Hz. Defaults to 1ppm.  
            skip (int): numnber of events which should be exluded at a run start.

        Returns:
            array(bool): selection of cycles with only good probes.
        """
        self.spk = [gm2.Spikes(self.raw_azi[:,probe], self.raw_freq[:,probe], self.spk_th) for probe in range(17)]
        self.spk_s = [gm2.np.abs(self.spk[probe].outl) < th for probe in range(17)]
        sall = self.spk_s[0]
        for p in range(17):
            if not(skip is None):
                if skip > 0:
                    self.spk_s[p][:skip] = False
            sall = sall&self.spk_s[p]
        self.s = sall
        return sall

    def loadFourier(self, N = 500, probes = None):
        """ Loads probe wise fourier fit in phi.

        Args: 
            N (int): number of coefficents, default 500.
            probes (list(int), optional): list with probes which should be loaded. If not specified all 17 probes are loaded. 
        """
        if probes is None:
            self.fr = [gm2.Fourier(self.raw_azi[self.s, probe]/180.*gm2.np.pi, self.raw_freq[self.s, probe], N) for probe in range(17)]
        else:
            self.fr = [None]*17
            for probe in probes:
                self.fr[probe] = gm2.Fourier(self.raw_azi[self.s, probe]/180.*gm2.np.pi, self.raw_freq[self.s, probe], N)

    def loadFreqAt(self, s = None, calib = None):
        """Loads the interpolation of the frequencies in selection.
        
        Args:
            s[nevents] (array(bool), optional): selection, if None the self.s the overall selection is used.
            calib[17] (array(float), optional): if provided it is used as calibration. 
                Otherwise standard calibration is used.
        """
        if calib is None:
            calib = self.tr.getCalibration()[0]
        s_ = [self.s&(self.raw_azi[:,probe] != 0)&(self.raw_freq[:, probe] > 0) for probe in range(17)]
        if self.raw_azi[:,0].std() > 0:
            self.freqAt = [gm2.util.interp1d(self.raw_azi[s_[probe], probe], self.raw_freq[s_[probe], probe] + calib[probe], fill_value='extrapolate') for probe in range(17)]

    def multipoles(self, n_ = 6, cov = None, s = None, data = None):
        """Calculates the multipoles with the provided covariance matrix up to order n.
        
        Args:
            n (int): multipole expansion order. Defaults to 6.
            cov[17x17] (optional): covariance matrix
            s[nevents] (bool, optional): special selection of events.

        Returns:
            m[nevents x n]: multipoles in Hz.
            res[nevents x 17]: residuals in Hz.
            chi2[nevent]: chi2s in Hz.
        """
        n = n_*2+1
        if cov is None:
            cov = gm2.np.diag([1.0]*17)
        if data is None:
            if self.freqAt is None:
                 self.loadFreqAt()
            if s is None:
                 s = self.s
            self.azi = self.raw_azi[s,8] 
            if self.freqAt:
                w = gm2.np.array([self.freqAt[probe](self.azi) for probe in range(17)]).T
            else:
                w = self.raw_freq[self.s]
        else:
            w = data
        nev = w.shape[0]
        trb = trBase()
        e   = cov
        M   = (gm2.np.linalg.pinv((trb[:,:n].T.dot(gm2.np.linalg.pinv(e)).dot( trb[:, :n])))).dot( trb[:,:n].T).dot(  gm2.np.linalg.pinv(e))
        peff = gm2.np.trace(trb[:, :n].dot(M))
        self.m    = gm2.np.zeros([nev, n],  dtype=float)
        self.res  = gm2.np.zeros([nev, 17], dtype=float)
        self.chi2 = gm2.np.zeros([nev],     dtype=float)
        for ev in range(nev):
            self.m[ev,:]   = M.dot(w[ev,:])
            self.res[ev,:] = (w[ev,:] - trb[:, :n].dot(self.m[ev,  :]))
            self.chi2[ev]  = self.res[ev,:].T.dot(gm2.np.linalg.pinv(e)).dot(self.res[ev,:])

        if data is None:
            self.mAt = [gm2.util.interp1d(self.azi, self.m[:,i], fill_value='extrapolate') for i in range(self.m.shape[1])]
        return self.m, self.res, self.chi2

    def dataResiduals(self, nsigma = None, N = None, th = None, skip = 7, plots = []):
        """Estimate the Residuals of Measuerements to the Fourier Fit. Optioanly truncated to data within nsigma.
        
        Args:
            nsigma (float, optional): if set only residuals within nsigma are returned. defaults None.
            N (int, optional): number of fourier coefficents. See loadFourier(), defaults to None.
            th (float, optional): spike threshold in Hz. See loadSpikes(), defaults to None. 
            plots (list(int)): list of probe numbers which should be ploted. Default [].

        Requires the load the fourier expansion which is done if it is not yet present.
        """
        if ((self.fr is None)|(not(N is None))|(not(th is None))):
            if th is None:
                self.loadSpikes(skip = skip)
            else:
                self.loadSpikes(th, skip = skip)
            if N is None: 
                self.loadFourier()
            else:
                self.loadFourier(N)

        data = [self.fr[probe].B(self.raw_azi[self.s, probe]/180*gm2.np.pi) - self.raw_freq[self.s, probe] for probe in range(17)]
        if nsigma is None:
            return data

        popt = []
        ss = gm2.np.full(data[0].shape, True)
        for probe in range(17):
            popt_, _ = gm2.plotutil.histWithGauss(gm2.plt.gca(), data[probe], bins=gm2.np.arange(-100, 100, 2), orientation='vertical', nsigma=nsigma, alpha=1.0)
            popt.append(popt_)
            if probe in plots:
                gm2.plt.xlabel("residuals [Hz]")
                gm2.plt.plot([popt_[1] - nsigma * gm2.np.abs(popt_[2]), popt_[1] - nsigma * gm2.np.abs(popt_[2])], [0, 300], '--', color=gm2.sns.color_palette()[2])
                gm2.plt.plot([popt_[1] + nsigma * gm2.np.abs(popt_[2]), popt_[1] + nsigma * gm2.np.abs(popt_[2])], [0, 300], '--', color=gm2.sns.color_palette()[2])
                gm2.despine()
                gm2.plt.show()
            gm2.plt.clf()
            s_ = (data[probe] > popt_[1] - nsigma * gm2.np.abs(popt_[2]) )&( data[probe] < popt_[1] + nsigma * gm2.np.abs(popt_[2]) )
            ss = ss&s_

        #cov_phi  = self.azi[self.s, 8][ss]
        data     = gm2.np.array(data)
        return data[:,ss]

    def plotFourier(self, probes):
        """Plots the fourier fit of probes (start at 0).

        Args:
            probes (array(int)): probe numbers to plot.

        Returns:
            f (figure): figure with plot.
        """
        if not(self.fr is None):
            f = gm2.plt.figure()
            for probe in probes:
                if not(self.fr[probe] is None):
                    gm2.plt.plot(self.raw_azi[self.s, probe], self.raw_freq[self.s, probe],'.',label="data #%i" % (probe+1))
                    gm2.plt.plot(gm2.np.sort(self.raw_azi[self.s, probe]), self.fr[probe].B(gm2.np.sort(self.raw_azi[self.s, probe])/180*gm2.np.pi), label="fit #%i" % (probe+1))
            gm2.plt.xlabel("azi [deg]")
            gm2.plt.ylabel("frequency [Hz]")
            gm2.despine()
            return f
        else:
            print("Please first loadFourier.")

    def dataCov(self, nsigma = None, N = None, th = None, skip = 7):
        """Data covariance matrix.
        
        Args:
            nsigma (float, optional): if set only residuals within nsigma are returned. defaults None.
            N (int, optional): number of fourier coefficents. See loadFourier(), defaults to None.
            th (float, optional): spike threshold in Hz. See loadSpikes(), defaults to None. 

        Requires the load the fourier expansion which is done if it is not yet present.
        """
        return gm2.np.cov(self.dataResiduals(nsigma=nsigma, N=N, th=th, skip=skip))

    def plotResolution(self, th=None, xlim=50, binw=1.0):

        def trlyHistWithGauss(ax, probe, data):
            gm2.plotutil.histWithGauss(gm2.plt.gca(), data[:,probe], bins=gm2.np.arange(-xlim, xlim, binw), orientation='vertical')
        data = gm2.np.array(self.dataResiduals(th=th)).T
        xlim = 100
        f = gm2.plotutil.trlyPlot(trlyHistWithGauss, xlim, "residuals [Hz]", "", data)
        return f

    def plotChi2(self):
        """Plot chi2 as a function of azimuth."""
        plotChi2(self.azi, self.chi2)

    def rms(self, freq=False, sel = None, show=None):
        """ Returns rms of the fieldmap. 

        Args:
            freq (bool,optional) : if True frequency instead of multipoles are used. Defaults to False.
             sel (ndarray(bool, n_azi), optioanl) : additional selection of multipoles or frequencies. Defaults to None.
             show (int, optional) : If not None the used data of probe `show` or `show`ed multipole is plotted. Defaults to False.

        Returns:
            np.array(float, [n_multipoles]) : rms of the multipoles (or frequencies if freq=True, in this case the dimension is n_probes
        """
        if sel is None:
            s_ = gm2.np.ones_like(self.azi) == 1
        else:
            s_ = sel
       
        if (type(show) == bool):
            if show:
                show = 0

        dazi = self.azi[s_][-1] - self.azi[s_][0]
        phi = (self.azi[s_][0]-gm2.np.arange(0,360-dazi, 0.001)+360.)%360
        if freq: 
            if not show is None:
                probe = show
                gm2.plt.plot(self.raw_azi[:,probe], self.raw_freq[:,probe],'.', markersize=3, label="raw")
                gm2.plt.plot(phi, self.freqAt[probe](phi),'.', markersize=2, label="sampling for rms")
                gm2.plt.xlabel("azimuth [deg]")
                gm2.plt.xlabel("frequency [hz]")
                gm2.plt.legend(markerscale=4)
                gm2.plt.title("Probe "+str(show+1))
                gm2.despine()
                gm2.plt.show()
            return gm2.np.array([self.freqAt[p](phi).std() for p in range(17)])
        else:
            if not show is None:
                gm2.plt.plot(self.azi, self.m[:,show],'.', markersize=3, label="raw")
                gm2.plt.plot(phi, self.mAt[show](phi),'.', markersize=2, label="sampling for rms")
                gm2.plt.xlabel("azimuth [deg]")
                gm2.plt.xlabel("frequency [hz]")
                gm2.plt.legend(markerscale=4)
                gm2.plt.title("Multipole "+str(show))
                gm2.despine()
                gm2.plt.show()
            return gm2.np.array([gm2.np.nanstd(self.mAt[m](phi)) for m in range(len(self.mAt))])

    def avgFourier(self, m=None, N = 500, show=False):
        """Calculate the multipole `m` average by fitting furst a Fourier expansion.
        
        Args:
            m (int, optioanl) : multipole number. If None all avaiable multipoles are returned. 0 Dipole. Defaults to None.
            N (int, optional) : number of fourier coefficents. See gm2.Fourier. Defaults to 500.
            show (bool, optioanl) : plots the fit. Defaults to False.

        Returns:
           
        """
        if m is None:
            print("This might take a while.")
            m_ = gm2.np.arange(self.m.shape[1])
        else:
            m_ = gm2.np.array([m])
        fr = [gm2.Fourier(self.azi/180.*gm2.np.pi, self.m[:,mm], N) for mm in m_]
        tt = gm2.np.arange(0,360,0.1)
        if show:
            for mi, m in enumerate(m_):
                gm2.plt.plot(self.azi, self.m[:,m],'.')
                gm2.plt.plot(tt, fr[mi].B(tt/180.*gm2.np.pi))
                gm2.plt.xlabel("azimuth [deg]")
                gm2.plt.ylabel("multipole [Hz]")
                gm2.despine()
                gm2.plt.show()
        data = [fr[j].B(tt/180.*gm2.np.pi).mean() for j in range(m_.shape[0])]
        if m is None:
            return gm2.np.array(data)
        else:
            return data

    def trlyWithin(self, start, end, closest=True):
        """Returns events with multipoles within range 'start' to 'stop'.
        
        Args:
            start (float) : start position in degrees.
            end (float) : end position in degrees.
            closest (bool, optional) : if True it returns the smaller part of the ring between 'start' and 'stop'.
            
        Returns (ndarray(bool, [n_mp])) : returns a selection array `s` of all multipoles within this range.
            """
        if end < start:
            start_ = end
            end_   = start
        else:
            start_ = start
            end_   = end
        if closest:
            if gm2.np.abs(start_ - end_ ) < 180.:
                 return (self.azi > start_)&(self.azi <= end_)
            else:
                 return (self.azi < start_)|(self.azi >= end_)
        else:
            if gm2.np.abs(start_ - end_ ) < 180.:
                return (self.azi < start_)|(self.azi >= end_)
            else:
                return (self.azi > start_)&(self.azi <= end_)

    def avgw(self, azi, freq):
        """Calculate weigthed average.

        Args:
            azi (ndarray(float, [N])) : azimuthla positions
            freq (ndarray(float, [N])) : value which should be averaged. Same dimension as azi.
         
        Returns:
            float: average
        """

        d_azi = (azi[2:] - azi[:-2])/2.
        d_azi[d_azi>90] = d_azi[d_azi>90] - 180.
        return (d_azi[:,None] * freq[1:-1,:]).sum(axis=0)/(d_azi.sum()) 


    def avg(self, mode='weighted', freq=False, sel = None, oscillationCorrection=False, driftCorrection=False):
        """

        Args:
            mode (str, optional) : 'weighted': delta_phi, 'resample': resample. Defaults to 'weighted'.
            freq (bool, optional): If true the avg frequency is returned instead od multipoles. Defaults to False.
            sel (ndarray(bool, n_azi), optioanl) : additional selection of multipoles or frequencies. Defaults to None.
        """
        if sel is None:
            #s_ = gm2.np.ones_like(self.azi) == 1
            s_ = gm2.np.isnan(self.m[:,0])==False
        else:
            s_ = sel

        if mode == 'weighted':
            d_azi = (self.azi[s_][2:] - self.azi[s_][:-2])/2.
            d_azi[d_azi>90] = d_azi[d_azi>90] - 180.
            if gm2.np.abs(d_azi.sum()) < 358:
                print("WARNING! Only %f degrees present" % gm2.np.abs(d_azi.sum()))
            else:
                print("Missing %.1f deg." % (360 - gm2.np.abs(d_azi.sum())))
            if freq:
                freq_ = gm2.np.array([self.freqAt[p](self.azi[s_]) for p in range(17)]).T
                #return (d_azi[:,None] * freq_[1:-1,:]).sum(axis=0)/(d_azi.sum())
            else:
                freq_ = self.m[s_,:]
                if oscillationCorrection:
                    if self.oscillationsAt is None:
                        self.loadOscillation()
                    freq_ = -(self.oscillationsAt(self.raw_time[self.s,8]))# - self.oscillationsAt(self.raw_time[1,8]))
                    freq_ = freq_[:,None] 
                if driftCorrection:
                    if self.d is None:
                        self.getDipoleCorrections(self.raw_time[100,0])
                    freq_[:,0] = freq_[:,0] + self.d[s_]
            return (d_azi[:,None] * freq_[1:-1,:]).sum(axis=0)/(d_azi.sum())
        if mode == 'resample':
            dazi = self.azi[s_][-1] - self.azi[s_][0]
            phi = (self.azi[s_][0]-gm2.np.arange(0,360-dazi, 0.001)+360.)%360
            if freq: 
                return gm2.np.array([self.freqAt[p](phi).mean() for p in range(17)])
            else:
                return gm2.np.array([self.mAt[m](phi).mean() for m in range(len(self.mAt))])


    def avgSpikeScan(self, show=[0], rel=False, dontShow=False, logx=False, freq=False, ths = None, ppb=False):
        if ths is None:
            ths = gm2.np.array([0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.25, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5, 5.0, 7.5, 10.0, 15.0, 20.0, 25.0, 30.0, 40.0, 50.0, 60.0, 70.0, 80.0, 90.0, 100., 200., 300.0, 400.0, 500, 600.0, 700.0, 800.0, 900.0, 1000, 1500, 2000, 3000, 5000, 7500, 10000])
        mps = []
        n   = []
        #if rel:
        #   avg = self.avg()
        for th in ths:
             self.spk_th = th
             self.loadSpikes(th)
             data_ = gm2.np.array([self.spk[p].freq[1:-1] for p in range(17)]).T
             self.multipoles( data=data_)
             self.azi = self.raw_azi[1:-1,8]
             #print self.azi.shape, self.m
             if freq:
                 mps.append(self.avgw(self.raw_azi[1:-1,8], data_))
             else:
                 mps.append(self.avg())
             n.append(gm2.np.argwhere(self.s).shape[0])
        mps = gm2.np.array(mps)
        for m in show:
            if ppb:
                f = gm2.HZ2PPB
            else:
                f = 1.0
            if rel:
                #gm2.plt.plot(ths, mps[:,m]-avg[m],'.')
                gm2.plt.plot(ths, (mps[:,m]-mps[-1,m])*f,'.')
            else:
                gm2.plt.plot(ths, mps[:,m]*f,'.')
            gm2.plt.xlabel("threshold [Hz]")

            if freq:
                gm2.plt.title("Probe %i" % (m+1))
                if rel:
                    if ppb:
                        gm2.plt.ylabel("difference [ppb]")
                    else:
                        gm2.plt.ylabel("difference [Hz]")
                else:
                    gm2.plt.ylabel("Dipole [Hz]")
            else:
                if m in [0]:
                    if rel:
                        gm2.plt.title("Dipole")
                        if ppb:
                            gm2.plt.ylabel("difference [ppb]")
                        else:
                            gm2.plt.ylabel("difference [Hz]")
                    else:
                         gm2.plt.ylabel("Dipole [Hz]")
                else:
                    if rel:
                        gm2.plt.title("Multipole (m%i)" % m)
                        if ppb:
                            gm2.plt.ylabel("difference [ppb @ 45mm]")
                        else:
                            gm2.plt.ylabel("difference [Hz @ 45mm]")
                    else:
                        if ppb:
                            gm2.plt.ylabel("Multipole [ppb @ 45mm]")
                        else:
                            gm2.plt.ylabel("Multipole [Hz @ 45mm]")
            gm2.despine()
            if logx:
                gm2.plt.xscale('log')
            if not dontShow:
                gm2.plt.show()
        return ths, mps, n

    def avgDrop(self, fraction, n=1000):
        """Varriations in average if we drop fraction of data.
        
        Args:
            fraction (float [0,1]) : fraction of data that should randomly be dropped.
            n (int) : number of trials. Defaults to 1000.
        """
        import random
        data = []               
        for i in range(n):
            s = gm2.np.ones_like(self.azi)==1
            #s[gm2.np.random.randint(0,s.shape[0], int(fraction*self.azi.shape[0]))] = False
            s[random.sample(range(0,s.shape[0]), int(fraction*self.azi.shape[0]))] = False
            data.append(self.avg(sel=s))
        return gm2.np.array(data).std(axis=0)

    def WedgeShims(self, show=True, shims=None, shims_pos = None, n=36, fixedPos=False, showShim=False, title=""):
        extend = 15 #degs
        n_ = n
        if shims is not None:
            n_ = shims.shape[0]
        import scipy.stats as stats
        sort = gm2.np.argsort(self.azi)
        azi_ = self.azi[sort]

        scale = 1./stats.norm.pdf(azi_, 0, extend).max()
        def shimmedField(shim_phi, shim_ppm):
          shim = gm2.np.zeros_like(azi_)
          for s in zip(shim_phi, shim_ppm):
            shim = shim + s[1]*stats.norm.pdf(azi_, s[0], extend) * scale + s[1]*stats.norm.pdf(azi_ + 360.0, s[0], extend) * scale + s[1]*stats.norm.pdf(azi_ -360.0, s[0], extend) * scale
            dipole = self.m[sort,0]*gm2.HZ2PPM + shim
          if showShim:
              return shim
          return dipole
       
        #cnt = 0
        def shimmedFieldRMS(shim_phi, shim_ppm):
            #global cnt
            #if cnt%10==0:
            #    print cnt
            #cnt += 1
            return rms(shimmedField(shim_phi, shim_ppm))
       
        def shimmedFieldRMS_free(paras):
            n_ = paras.shape[0]/2
            return shimmedFieldRMS(paras[:n_], paras[n_:])

        def shimmedFieldRMS_fixed(paras): # phase + shim strengths
            return shimmedFieldRMS(shim_phi+paras[0], paras[1:])

        def rms(dipole):
             dipole_f = gm2.util.interp1d(azi_,dipole, fill_value='extrapolate')
             phi_sample = gm2.np.arange(0,360,0.001)
             return gm2.np.nanstd(dipole_f(phi_sample))

        if shims is None:
          ## if no shims are supplied run the optimazation
          
          
          import scipy.optimize as opt
          shim_phi = gm2.np.arange(0,360,360./n_)
          if fixedPos:
            ## use equaly spaced shim positions

            shim0 = gm2.np.zeros(shim_phi.shape[0]+1) # phase + shim strengths
            #shim0 = 0.
            bounds = [(-200,200) for a in range(n_ + 1)]
            bounds[0] = (0.0, 360/n)
            shim_out  = opt.minimize(shimmedFieldRMS_fixed, shim0, bounds=bounds).x
            shim = shim_out[1:]# - shim_out[1:].mean()
            shim_phi = shim_phi + shim_out[0]
          else:
            shim0 = gm2.np.concatenate((shim_phi, gm2.np.zeros_like(shim_phi)))
            bounds = [(0,360) for a in range(n_)] + [(-200,200) for a in range(n_)]
            shim_out  = opt.minimize(shimmedFieldRMS_free, shim0, bounds=bounds).x
            shim_phi = shim_out[:n_]
            shim     = shim_out[n_:]# - shim_out[n_:].mean()
        else:
          shim_phi = shims_pos
          shim = shims


        print shim_phi, shim
        if show:
          figsize = gm2.plotutil.figsize()
          gm2.plt.figure(figsize=[figsize[0]*2, figsize[1]])
        if not showShim:
          gm2.plt.plot(azi_, self.m[sort,0]*gm2.HZ2PPM, '.', markersize=2, label="input %.1fppm" % (rms(self.m[sort,0]*gm2.HZ2PPM)))
          gm2.plt.plot(azi_, shimmedField(shim_phi,shim), '-', markersize=2, label="prediction %.1fppm" % shimmedFieldRMS(shim_phi, shim))
          gm2.plt.ylabel("dipole [ppm]")
        else:
          gm2.plt.plot(azi_, shimmedField(shim_phi,shim), '-', markersize=2)
          gm2.plt.ylabel("shimming [ppm]")
        gm2.plt.xlabel("azimuth [deg]")
        gm2.plt.title(title)
        gm2.plt.legend(markerscale=6)
        if show:
            gm2.despine()
            gm2.plt.show()
        return shim_phi,shim


    def plotDipole(self, show=True, marker='-', n=0):
        """Plot the dipole as a function of azimuth.

        Args:
            show (bool): show the plot
            marker (str): plot marker style
            m (int) : multipole to plot. Defaults to 0: Dipole.
        """
        sort = gm2.np.argsort(self.azi) 
        gm2.plt.plot(self.azi[sort], self.m[sort,n]*gm2.HZ2PPM, marker, markersize=2, label="run %i" % self.runs[0])
        gm2.plt.xlabel("azimuth [deg]")
        if n in [0]:
          gm2.plt.ylabel("dipole [ppm]")
        if show:
            gm2.despine()
            gm2.plt.show()

    def posUncertenty(self, res=2., n=100, show=False, mno = 0, barrel=False):
        #phi = gm2.np.arange(0.0, 360.0, 5./124.) 
        #dp = self.mAt[0](phi)
        s = (gm2.np.abs(gm2.np.diff(self.azi))<5)&(gm2.np.abs(gm2.np.diff(self.azi))>0.01)
        dd = gm2.np.diff(self.azi)[s] 
        #if show:
        #    gm2.plt.plot(self.azi,self.m[:,0],'.')
        #    gm2.plt.plot(phi, dp,'.')
        #    gm2.plt.xlabel("azimuth [deg]")
        #    gm2.plt.ylabel("dipole [ppm]")
        #    gm2.despine()
        #    gm2.plt.show()
        def avgPos(phi, dp):
            dphi = (phi[1:] - phi[:-1])[s]
            return avg(dphi, dp)

        def avg(dd, dp):
            dphi = dd#phi[1:] - phi[:-1]
            #p  rint dphi.sum()
            #print dphi.sum()
            return (dp*dphi).sum()/dphi.sum()
        avgs = []
        avgs2 = []
        for i in range(n):
            if barrel:
               if res < 0:
                   barrel_factor = gm2.np.array([ 1.60944784e-04, -7.07829476e-04, -3.06749044e-04,  0,
                               9.55696649e-04, -7.47088459e-05,  1.78203401e-06, -5.10912502e-04,
                                      -1.77929349e-04,  1.21594296e-03, -1.30319980e-05, -5.43205207e-04])+ 1.0
               else:
                  barrel_factor = gm2.np.random.normal(0, res, 12) + 1.
                  barrel_factor[-1] = 1-(barrel_factor[:-1]-1).sum()
               #print barrel_factor.sum()
               dd_ = gm2.np.copy(dd)
               for i, f in enumerate(barrel_factor):
                   azi_ = (self.azi[1:][s]+15.)%360.
                   ss = (azi_> i*30.)&(azi_ <=(i+1)*30.)
                   dd_[ss] = dd[ss] * f
            else:
               phi_ = (self.azi + gm2.np.random.normal(0, res/124.,self.azi.shape[0]))
               dd_ = dd + gm2.np.random.normal(0, res/124., dd.shape[0])
               dd_[dd_ > 0] = 0.0 
            dp = (self.m[:-1, mno][s] + self.m[1:, mno][s])/2.
            avgs.append(avg(dd_, dp))
            #print avgs[-1]
            if not barrel:
                avgs2.append(avgPos(phi_, dp)) 

        print gm2.np.array(avgs).std(), gm2.np.array(avgs).mean() 
        if barrel:
            return gm2.np.array(avgs).std()
        print gm2.np.array(avgs2).std(), gm2.np.array(avgs2).mean() 
        return gm2.np.array(avgs2).std()

    def loadOscillation(self):
        self.fp = gm2.FixedProbe(self.runs, True, prefix=self.prefix)
        probe=100
        s = self.fp.time[:,probe] > 0
        mean = gm2.np.mean(self.fp.freq[:,self.fp.select(radIds=['M'])].mean(axis=1)[s])
        self.oscillationsAt = gm2.util.interp1d(self.fp.time[s,probe], self.fp.freq[:,self.fp.select(radIds=['M'])].mean(axis=1)[s]-mean, fill_value='extrapolate')
        #mean

    class Drift:
        def __init__(self, phi, phis, weights, trly_times, fp_times, trly_i, fp_i, station, popt):
            self.phi           = phi
            self.phis          = phis
            self.weights       = weights
            self.trly_times    = trly_times
            self.fp_times      = fp_times
            self.trly_integral = trly_i
            self.fp_integral   = fp_i
            self.station       = station
            self.popt          = popt
            self.offsets       = self.fp_integral - self.trly_integral[:,0]

        def dipole(self, phi, time):
            station_no = self.getStation(phi)
            return [self.station[station_no], self.fp_times[station_no], self.fp_integral[station_no]]

        def getVirtualTrolley(self,time):
            pass
            # TODO

        def getStationPair(self, phi):
            station = self.getStation(phi)
            dphi = phi - self.phi[station]
            if dphi < -180:
                dphi =+ 360.
            if dphi > 180:
                dphi =- 360. 
            if dphi > 0:
                station2 = (station + 1)%72
            else:
                station2 = (station-1+72)%72
            dphi2 = phi -  self.phi[station2]
            print dphi2
            if dphi2 < -180:
                dphi2 =+ 360.
            if dphi2 > 180:
                dphi2 = dphi2 -  360. 
            return station, station2, dphi, dphi2 

        def getStation(self, phi):
            if ((phi < 0.)|(phi>360.)):
                raise Exception('phi should be >= 0 and <= 360. The valuewas: {}'.format(phi))
            idx = gm2.np.argwhere((self.phis[:,0]<phi)&(self.phis[:,1]>=phi))
            if idx.shape[0] == 1:
                return idx[0,0]
            if idx.shape[0] > 1:
                raise Exception('Could not match the corresponding fixed probe station for phi {}'.format(phi))
            else:
                return 2

        def dipoleAt(self, phi, time):
            station_no = self.getStation(phi)
            return gm2.util.func_lin(time, *self.popt[station_no])

    def getDipoleCorrection(self, phi, t, time):
        if self.drift == None:
            self.loadDriftCorrection()
        #station, fp_time, offset =  self.drift.dipole(phi, time)
        station = self.drift.station[self.drift.getStation(phi)]
        s_station = self.fp.select(yokes=[station[0]], aziIds=[station[1]])
        s_dipole = self.fp.select(radIds=['M'])
        fp_t = []
        for idx in gm2.np.argwhere((s_station&s_dipole)):
             fp_t.append(self.fp.freqAtTime[idx[0]](time))
        fp_t = gm2.np.mean(fp_t)
        ref = self.drift.dipoleAt(phi, t)
        correction = gm2.np.array(fp_t - ref)
        correction[gm2.np.abs(correction)>50] = 0
        return correction 
       
    def getDipoleCorrections(self, time, show=True):
        self.d = gm2.np.zeros([self.m.shape[0]])
        m_times = self.raw_time[self.s, 8]
        for i, phi in enumerate(self.azi):
            self.d[i] = self.getDipoleCorrection(phi, m_times[i], time)
        if show:
            gm2.plt.plot(self.azi, self.d,'.')
            gm2.plt.xlabel("azimuth [Hz]")
            gm2.plt.ylabel("dipole correction [Hz]")
            #gm2.despine()
            gm2.plt.show()
        return self.d

    def loadDriftCorrection(self, show=False):
        offset = - 2.0 #deg
        trly_opffset = 5.0
        def loadStations():
            self.loadOscillation()
            phi = self.fp.getStationPhi()
            station_phis = []
            for i in range(72):
                 last = phi[i-1] - phi[i] 
                 nex_ = phi[(i+1)%72] - phi[i]
                 if last > 0:
                     last = - (360-phi[i-1] + phi[i])
                 if nex_ < 0:
                     nex_ = phi[(i+1)%72] + 360 - phi[i]
                 station_phis.append([(last/2. + phi[i] + 360. + offset) % 360., (nex_/2. + phi[i] + offset) % 360. ])
            return gm2.np.array(station_phis)
        station_phis = loadStations()
        station_weight = station_phis[:,1] - station_phis[:,0]
        station_weight[station_weight<0] = station_weight[station_weight<0] + 360
        station = self.fp.getStations()
        station_trly_times  = gm2.util.interp1d(self.azi, self.raw_time[self.s,8])(station_phis)
        station_trly_times_ = gm2.util.interp1d(self.azi, self.raw_time[self.s,8])((station_phis + trly_opffset)%360)
        station_fp_time  = []
        station_integral = []
        trolley_integral = []
        popts            = []
        probe = 100
        s_dipole = self.fp.select(radIds=['M'])
        for i,s in enumerate(station):
             s_station = self.fp.select(yokes=[s[0]], aziIds=[s[1]])
             s_time    = ((self.fp.time[:,probe] > station_trly_times[i][0])&(self.fp.time[:,probe] <= station_trly_times[i][1]))|((self.fp.time[:,probe] < station_trly_times[i][0])&(self.fp.time[:,probe] >= station_trly_times[i][1]))
             s_time_    = ((self.fp.time[:,probe] > station_trly_times_[i][0])&(self.fp.time[:,probe] <= station_trly_times_[i][1]))|((self.fp.time[:,probe] < station_trly_times_[i][0])&(self.fp.time[:,probe] >= station_trly_times_[i][1]))
             s_station_dipole  = s_station & s_dipole
             station_integral.append((self.fp.freq[:,s_station_dipole][s_time,:][-10:]).mean())
             trolley_integral.append(self.avg(sel=self.trlyWithin(station_phis[i][0], station_phis[i][1])))
             station_fp_time.append(self.fp.time[:,probe][s_time][-10:].mean())
             #print gm2.np.argwhere(s_station).shape, gm2.np.argwhere(s_time).shape
            

             fit_t  = gm2.np.concatenate([self.fp.time[:,s_station_dipole][s_time,:][-10:].mean(axis=1),
                                          self.fp.time[:,s_station_dipole][s_time_,:][:10].mean(axis=1)])
             fit_dp = gm2.np.concatenate([self.fp.freq[:,s_station_dipole][s_time,:][-10:].mean(axis=1),
                                          self.fp.freq[:,s_station_dipole][s_time_,:][:10].mean(axis=1)])
             popt = gm2.util.fit_lin(fit_t, fit_dp)
             popts.append(popt)
             if show:
                 #gm2.plotutil.plot_ts(self.fp.time[:,s_station_dipole][1:,:], self.fp.freq[:,s_station_dipole][1:,:],'.')
                 #gm2.plotutil.plot_ts(self.fp.time[:,s_station_dipole][s_time,:], self.fp.freq[:,s_station_dipole][s_time,:],'.')
                 #gm2.plotutil.plot_ts(self.fp.time[:,s_station_dipole][s_time,:][-10:], self.fp.freq[:,s_station_dipole][s_time,:][-10:],'.')
                 #gm2.plotutil.plot_ts(self.fp.time[:,s_station_dipole][s_time_,:][:10], self.fp.freq[:,s_station_dipole][s_time_,:][:10],'.')
                 
                 gm2.plotutil.plot_ts(self.fp.time[:,s_station_dipole][1:,:].mean(axis=1), self.fp.freq[:,s_station_dipole][1:,:].mean(axis=1),'.')
                 gm2.plotutil.plot_ts(self.fp.time[:,s_station_dipole][s_time,:].mean(axis=1), self.fp.freq[:,s_station_dipole][s_time,:].mean(axis=1),'.')
                 gm2.plotutil.plot_ts(fit_t, fit_dp,'.')

                 gm2.plotutil.plot_ts(fit_t, gm2.util.func_lin(fit_t, *popt),'-')
                 if i%6==5:
                     gm2.plt.show()
        

        trolley_integral = gm2.np.array(trolley_integral)
        self.drift = gm2.FieldMap.Drift((self.fp.getStationPhi()+offset+360.)%360., station_phis, station_weight, station_trly_times, station_fp_time, trolley_integral, station_integral, station, popts)
        
    def txt(self, m=[0,1,2,3,4], fname="",delimiter=","):
        azi = (self.azi + 360.)%360.
        if fname == "":
            gm2.np.savetxt("trolley_"+str(self.runs[0])+".csv", gm2.np.concatenate((azi[:,None], self.m[:,m]),axis=1), fmt="%.3f", delimiter=delimiter)
        else:
            gm2.np.savetxt(fname, gm2.np.concatenate((azi[:,None], self.m[:,m]),axis=1), fmt="%.3f",delimiter=delimiter)

    def plot(self, m=[0], show=True, label="", title=""):
        if show:
            figsize = gm2.plotutil.figsize()
            gm2.plt.figure(figsize=[figsize[0]*2, figsize[1]])
        for m_ in m:
            azi = (self.azi + 360.)%360.
            if label == "":
                gm2.plt.plot(azi, self.m[:,m_], '.', markersize=2, label="m%i" % (m_+1))
            else:
                gm2.plt.plot(azi, self.m[:,m_], '.', markersize=2, label=label)
        gm2.plt.xlabel("azimuth [deg")
        gm2.plt.ylabel("multipole [Hz]")
        if title != "":
            gm2.plt.title(title)
        if show:
            gm2.plt.legend(markerscale=4)
            gm2.despine()
            gm2.plt.show()

def printMultipoleResolution(cov, modes = ['cor', 'dia', 'uni']):
    """Print latex table of multipole resolutions of the specified modes.
    
    Args:
        cov (array(17x17)): data covariance matrix.
        modes (list['cor', 'dia', 'uni']): specify used modes.
            'cor'  full corelations, 'dia' only different probe resolutions, 'uni' uniform resolution.
    """
    e   = cov
    e_d = gm2.np.diag(gm2.np.diag(cov))
    e_a = gm2.np.diag(gm2.np.ones([17])*(gm2.np.sqrt(gm2.np.diag(cov)).mean()**2))
    trb = trBase()
    for n in range(1,18,2):
        if 'cor' in modes:
            cov_   = (gm2.np.linalg.pinv((trb[:,:n].T.dot(gm2.np.linalg.pinv(e)).dot(trb[:, :n]))))
            print(gm2.np.array_str(gm2.np.sqrt(gm2.np.diag(cov_)), precision=3, max_line_width=10000).replace(". ", "& ").replace("nan","-&").replace(".]","\\\\").replace("[","cor & %i &" % (n//2)))
        if 'dia' in modes:
            cov_d = (gm2.np.linalg.pinv((trb[:,:n].T.dot(gm2.np.linalg.pinv(e_d)).dot(trb[:, :n]))))
            print(gm2.np.array_str(gm2.np.sqrt(gm2.np.diag(cov_d)),precision=3, max_line_width=10000).replace(". ", "& ").replace("nan","-&").replace(".]","\\\\").replace("[","dia & %i &" % (n//2)))
        if 'uni' in modes:
            cov_a = (gm2.np.linalg.pinv((trb[:,:n].T.dot(gm2.np.linalg.pinv(e_a)).dot(trb[:, :n]))))
            print(gm2.np.array_str(gm2.np.sqrt(gm2.np.diag(cov_a)),precision=3, max_line_width=10000).replace(". ", "& ").replace("nan","-&").replace(".]","\\\\").replace("[","uni & %i &" % (n//2)))

def printMultipoleResolution(cov, n):
    """ Plot multipole resolutions and correlations of the specified modes.
    
    Args:
        cov (array(17x17)): data covariance matrix.
        n (int): expansion, needs to be in range(1,18,2).
    """
    trb = trBase()
    e   = cov
    e_d = gm2.np.diag(gm2.np.diag(cov))
    e_a = gm2.np.diag(gm2.np.ones([17])*(gm2.np.sqrt(gm2.np.diag(cov)).mean()**2))
    cov   = (gm2.np.linalg.pinv((trb[:,:n].T.dot(gm2.np.linalg.pinv(e)).dot(trb[:, :n]))))
    cov_d = (gm2.np.linalg.pinv((trb[:,:n].T.dot(gm2.np.linalg.pinv(e_d)).dot(trb[:, :n]))))
    cov_a = (gm2.np.linalg.pinv((trb[:,:n].T.dot(gm2.np.linalg.pinv(e_a)).dot(trb[:, :n]))))

    ''' plot the fit error and correlations '''
    allTypes = True
    fs = gm2.plotutil.figsize()
    cs = gm2.sns.color_palette('Spectral_r',40)
    f = gm2.plt.figure(figsize=[fs[0]*1.5, fs[1]*1.5])
    ax = []
    cov_ = cov
    cor_ = cov_ / gm2.np.sqrt(gm2.np.diag(cov_)[:,None] * gm2.np.diag(cov_))
    t = gm2.np.arange(0, 2 * gm2.np.pi, 0.01)
    for i in range(n):
        for j in range(n):
            ax.append(gm2.plt.subplot2grid((17, 17), (i, j)))
            ax[-1].xaxis.set_ticklabels([])
            ax[-1].yaxis.set_ticklabels([])
            ax[-1].xaxis.set_ticks([])     
            ax[-1].yaxis.set_ticks([])
            if i == j:
                 ax[-1].text(0.5,0.5,"%.2f" % gm2.np.sqrt(cov_[i,j]), horizontalalignment='center', verticalalignment='center', fontsize=10)
                 if i in [0]:
                     ax[-1].set_title(r"$B_{0}$")
                     ax[-1].set_ylabel(r"$B_{0}$")
            if j > i:
                w, v = gm2.np.linalg.eig(cov_[[i,j]][:,[i,j]])
                xx = []
                for y in (gm2.np.array([gm2.np.cos(t), gm2.np.sin(t)]).T * gm2.np.sqrt(w)):
                     xx.append(v.dot(y))
                xx = gm2.np.array(xx)
                ax[-1].plot(xx[:,1], xx[:,0])
                if allTypes:
                    w_d, v_d = gm2.np.linalg.eig(cov_d[[i,j]][:,[i,j]])
                    xx_d = []
                    w_a, v_a = gm2.np.linalg.eig(cov_a[[i,j]][:,[i,j]])
                    xx_a = []
                    for y in (gm2.np.array([gm2.np.cos(t), gm2.np.sin(t)]).T * gm2.np.sqrt(w_d)):
                        xx_d.append(v_d.dot(y))
                    for y in (gm2.np.array([gm2.np.cos(t), gm2.np.sin(t)]).T * gm2.np.sqrt(w_a)):
                        xx_a.append(v_a.dot(y))
                    xx_d = gm2.np.array(xx_d)
                    xx_a = gm2.np.array(xx_a)
                    ax[-1].plot(xx_d[:,1], xx_d[:,0])
                    ax[-1].plot(xx_a[:,1], xx_a[:,0])
                ax[-1].set_xlim([-30,30])
                ax[-1].set_ylim([-30,30])
                if i in [0]:
                    if j%2 == 0:
                        ax[-1].set_title(r"$b_{%i}$" % ((j+1)//2) )
                    else:
                        ax[-1].set_title(r"$a_{%i}$" % ((j+1)//2) )
            if j < i:
                ax[-1].text(0.5,0.5,"%.2f" % cor_[i,j], horizontalalignment='center', verticalalignment='center', fontsize=10)
                if ~gm2.np.isnan(cor_[i,j]):
                    ax[-1].set_facecolor(cs[int(gm2.np.floor(cor_[i,j]*20))+20 ])
                #else:
                #    ax[-1].set_facecolor(cs[int(np.floor(cor_[i,j]*20))+20 ])
                if j in [0]:
                    if i%2 == 0:
                        ax[-1].set_ylabel(r"$b_{%i}$" % ((i+1)//2) )
                    else:
                        ax[-1].set_ylabel(r"$a_{%i}$" % ((i+1)//2) )

    gm2.plt.subplots_adjust(wspace=0, hspace=0)
    #plt.savefig("plots/mp_coef_cor_%i.png" % (i//2))
    #plt.savefig("plots/mp_coef_cor_%i.pdf" % (i//2))
    return f


def trBase(at=45.0):
    """Get Trolley probe value in multipole base.
    
    Args:
        at (float): multipole evaluation distance in mm. Defaults to 45mm."""
    pos_fp = (gm2.FP.probes.position.r, gm2.FP.probes.position.theta)
    pos_tr = (gm2.TR.probes.position.r, gm2.TR.probes.position.theta)

    trb  = gm2.np.array([gm2.util.multipole(pos_tr, 1,     0,    0),
             gm2.util.multipole(pos_tr, 0, 1./45,    0),
             gm2.util.multipole(pos_tr, 0,    0, 1./45),
             gm2.util.multipole(pos_tr, 0,    0,     0, 1./45**2,     0),
             gm2.util.multipole(pos_tr, 0,    0,     0,     0, 1./45**2),
             gm2.util.multipole(pos_tr, 0,    0,     0,     0,      0, 1./45**3,     0),
             gm2.util.multipole(pos_tr, 0,    0,     0,     0,      0,     0, 1./45**3),
             gm2.util.multipole(pos_tr, 0,    0,     0,     0,      0,     0,     0, 1./45**4,     0),
             gm2.util.multipole(pos_tr, 0,    0,     0,     0,      0,     0,     0,     0, 1./45**4),
             gm2.util.multipole(pos_tr, 0,    0,     0,     0,      0,     0,     0,     0,     0, 1./45**5,     0),
             gm2.util.multipole(pos_tr, 0,    0,     0,     0,      0,     0,     0,     0,     0,     0, 1./45**5),
             gm2.util.multipole(pos_tr, 0,    0,     0,     0,      0,     0,     0,     0,     0,      0,     0, 1./45**6,     0),
             gm2.util.multipole(pos_tr, 0,    0,     0,     0,      0,     0,     0,     0,     0,      0,     0,     0, 1./45**6),
             gm2.util.multipole(pos_tr, 0,    0,     0,     0,      0,     0,     0,     0,     0,      0,     0,     0,     0, 1./45**7,     0),
             gm2.util.multipole(pos_tr, 0,    0,     0,     0,      0,     0,     0,     0,     0,      0,     0,     0,     0,     0, 1./45**7),
             gm2.util.multipole(pos_tr, 0,    0,     0,     0,      0,     0,     0,     0,     0,      0,     0,     0,     0,     0,     0, 1./45**8,     0),
             gm2.util.multipole(pos_tr, 0,    0,     0,     0,      0,     0,     0,     0,     0,      0,     0,     0,     0,     0,     0,     0, 1./45**8)]).T

    trb[:,12] = 0.0
    return trb 

def plotFieldMapUncertainty(cov, N):
    """Plot field map uncertenty for expansion up to N.
    
    Args:
        cov (array(17x17)): data covariance matrix.
        N (int): multipole expansion up to N. Should be in range(1,18,2).
    """
    trb = trBase()
    e   = cov
    cov   = (gm2.np.linalg.pinv((trb[:,:N].T.dot(gm2.np.linalg.pinv(e)).dot(trb[:, :N]))))

    mm = 50.
    xx, yy = gm2.np.meshgrid(gm2.np.arange(-mm, mm), gm2.np.arange(-mm, mm))
    rr  = gm2.np.sqrt(xx**2 + yy**2)
    phi = gm2.np.arctan2(yy, xx)
    s2 = gm2.np.zeros_like(xx)
    for i in range(rr.shape[0]):
        for j in range(rr[0].shape[0]):
            s2[i,j]   = f(rr[i,j], phi[i,j], N//2).dot(cov).dot(f(rr[i,j], phi[i,j], N//2).T)

    step = 50 # ppb
    names = ["cor","dia","uni"]
    for ii, s2_ in enumerate([s2]):
        ff = gm2.plt.figure(figsize=(7, 6))
        from matplotlib.colors import ListedColormap
        s2_[rr>mm] = gm2.np.nan
        nmax = 1000
        if nmax is None:
            nmax = ((gm2.np.ceil(gm2.np.nanmax(gm2.np.sqrt(s2_)))*gm2.HZ2PPB)//step+1)*step
            nmax = gm2.np.min([nmax, 1000])
        nmin = 0
        if nmin is None:
            nmin = ((gm2.np.floor(gm2.np.nanmin(gm2.np.sqrt(s2_)))*gm2.HZ2PPB)//step)*step
        nc = int((nmax-nmin)//step)
        cmap = ListedColormap(gm2.sns.color_palette("Spectral_r", nc))
        #cmap = ListedColormap(gm2.sns.color_palette("Blues", nc))
        levels_ = gm2.np.arange(nmin, nmax+(nmax-nmin)/nc, step)
        cs = gm2.plt.contour(xx, yy, gm2.np.sqrt(s2_)*gm2.HZ2PPB,  levels=levels_, colors='k', linewidths=1.0, inline=True, alpha=0.5)
        gm2.plt.clabel(cs, inline=1, fontsize=10)
        cf = gm2.plt.contourf(xx, yy, gm2.np.sqrt(s2_)*gm2.HZ2PPB, levels=levels_, cmap=cmap, extend='max')
        cb = gm2.plt.colorbar()
        cb.ax.set_title("[ppb]")
        gm2.plt.plot(gm2.TR.probes.position.x, gm2.TR.probes.position.y, 'x', color='black', alpha=0.3)
        t = gm2.np.arange(0, gm2.np.pi*2,0.01)
        r_ = 45.
        gm2.plt.plot(r_*gm2.np.sin(t), r_*gm2.np.cos(t),color='black', alpha=0.3, linewidth=2)

        gm2.plt.xlim([-mm, mm])
        gm2.plt.ylim([-mm, mm])
        gm2.plt.xlabel("x [mm]")
        gm2.plt.ylabel("y [mm]")
        gm2.plt.title(r"$n_{\rm{max}}=%i$, %s" % (N//2, names[ii]))
        gm2.plt.tight_layout()
    return ff

def f(r, phi, n, r0=45.):
    """Calculate multipole term n at positions r(array), phi(array).
    
    Args:
        r (floar ot array(float)): radius of point.
        phi (floar ot array(float)): phi of point, same dimension as r.
        n (array(float)): number of multipole term. 0:Dipole, 1: skew qud, ...
        """
    if type(r) is not gm2.np.ndarray:
        r   = gm2.np.array([r])
        phi = gm2.np.array([phi])
    if r.shape[0] != phi.shape[0]:
        raise Exception('Dimension of r and phi have to be the samne.')
    ii = gm2.np.arange(1,n+1)
    return gm2.np.concatenate([gm2.np.ones([r.shape[0], 1]),
                           gm2.np.insert((((r[:,None]/r0)**ii) * gm2.np.sin(ii * phi[:,None])) ,gm2.np.arange(n),
                                     (((r[:,None]/r0)**ii) * gm2.np.cos(ii * phi[:,None])) , axis=1)], axis=1)

def plotDataCov(residuals):
    cor = gm2.np.corrcoef(residuals)
    """Helper Function to plot data covariance and correlations
    
    Args:
        residuals (array(17x..)): residual data.
    """
    fs = gm2.plotutil.figsize()
    f = gm2.plt.figure(figsize=[fs[0]*1.5, fs[1]*1.5])
    ax = []
    ax.append(gm2.plt.subplot2grid((17, 17), (0, 0)))
    ax[-1].xaxis.set_ticklabels([])
    ax[-1].yaxis.set_ticklabels([])
    ax[-1].xaxis.set_ticks([])
    ax[-1].yaxis.set_ticks([])
    gm2.plt.title("1")
    ax[-1].set_ylabel("1")

    ax.append(gm2.plt.subplot2grid((17, 17), (16, 16)))
    ax[-1].xaxis.set_ticklabels([])
    ax[-1].yaxis.set_ticklabels([])
    ax[-1].xaxis.set_ticks([])
    ax[-1].yaxis.set_ticks([])

    cs = gm2.sns.color_palette('Blues',10)
    cs = gm2.sns.color_palette('Spectral_r',40)
    for p1 in range(0,17):
        for p2 in range(p1+1,17):
           ax.append(gm2.plt.subplot2grid((17, 17), (p1, p2)))
           ax[-1].plot(residuals[p1,:], residuals[p2,:], '.', markersize=2, alpha=0.1)
           ax[-1].set_xlim([-70,70])
           ax[-1].set_ylim([-70,70])
           ax[-1].xaxis.set_ticklabels([])
           ax[-1].yaxis.set_ticklabels([])
           ax[-1].xaxis.set_ticks([])
           ax[-1].yaxis.set_ticks([])
           if p1 in [0]:
               gm2.plt.title("%i" % (p2+1))
        for p2 in range(0, p1):
           ax.append(gm2.plt.subplot2grid((17, 17), (p1, p2)))
           if p2 in [0]:
              ax[-1].set_ylabel("%i" % (p1+1))
           ax[-1].xaxis.set_ticklabels([])
           ax[-1].yaxis.set_ticklabels([])
           ax[-1].xaxis.set_ticks([])
           ax[-1].yaxis.set_ticks([])
           ax[-1].text(0.5,0.5,"%.2f" % cor[p1,p2], horizontalalignment='center', verticalalignment='center', fontsize=8)
           ax[-1].set_facecolor(cs[int(gm2.np.floor(cor[p1,p2]*40)) ])

    gm2.plt.subplots_adjust(wspace=0, hspace=0)
    return f

from scipy import stats
def plotChi2Dist(chi2, peff = None, cov = None, label = ""):
    """Plot chi2 distribution.
    
    Args:
        chi2 (array): hi2s array.
        peff (float, optional): degree of freedoms of M matrix.
        cov[17x17] (optional): covariance matrix to determine d.o.f.
        label (str, optional): plot lable.
    """
    if peff is None:
        if not(cov is None):
            e = cov
            trb = trBase()
            M   = (gm2.np.linalg.pinv((trb[:,:n].T.dot(gm2.np.linalg.pinv(e)).dot( trb[:, :n])))).dot(trb[:,:n].T).dot(gm2.np.linalg.pinv(e))
            peff = gm2.np.trace(trb[:, :n].dot(M))
    nlim = 200
    lim = chi2.mean()
    v  , _, _ = gm2.plt.hist(chi2, bins=gm2.np.arange(0,lim, lim/nlim), histtype='step', label=label)
    tt = gm2.np.arange(0, lim, lim/nlim/2.)
    if not(peff is None):
        chi2_ = stats.chi2.pdf(tt,17.-peff)
        gm2.plt.plot(tt, chi2_/chi2_.max()*v.max()*0.8, '-', label=r"$\chi^2$(d.o.f.= %.1f)" % (17.-peff))
    gm2.plt.xlabel(r"$\chi^{2}$")
    #plt.title(r"$n=%i$" % (n))
    gm2.plt.legend()
    gm2.despine()
    gm2.plt.show()

def plotChi2(azi, chi2):
    """Plot chi2 as a function of azimuth.
    
    Args:
        azi[nevents] (array): azimuthal position.
        chi2[nevents] (array): chi2s.

    Returns:
        fig (figure): figure.
    """
    fs = gm2.plotutil.figsize()
    fig = gm2.plt.figure(figsize=[fs[0]*1.5, fs[1]])
    gm2.plt.plot(azi, chi2,  '.', markersize=2, alpha=1,label="cor")
    gm2.plt.xlabel("azimuth [rad]")
    gm2.plt.ylabel("$\chi^2$")
    gm2.despine()
    return fig

def study(self):
    fp = gm2.FixedProbe(self.runs,load='simple')
    s_phi = fp.getStationPhi()
    s_phi = np.sort(s_phi)
    az = np.arange(0,360,0.001)
    d_s_boxcar = []
    d_s_linear = []
    for n, sphi in enumerate(s_phi):
        if n in [0]: 
           lower = s_phi[-1]  
        else:
           lower = s_phi[n-1]
        if n in [s_phi.shape[0]-1]:
           upper = s_phi[0]
        else:
           upper = s_phi[n+1]
        s = (az>lower)&(az<upper)
        if lower > upper:
            s = (az>lower)|(az<upper)
       
        az_    = ((az[s] - sphi+180.)%360)-180.
        lower_ = ((lower - sphi+180.)%360)-180.
        upper_ = ((upper - sphi+180.)%360)-180.
        
        w_boxcare = (az_>lower_/2.)&(az_<upper_/2.)
        print(az_.min(), az_.max(), lower_, upper_)

        w_linear = np.ones_like(az_)
        w_linear[az_<0] = w_linear[az_<0] - az_[az_<0]/az_.min() 
        w_linear[az_>0] = w_linear[az_>0] - az_[az_>0]/az_.max()
        #gm2.plt.plot(az[s], w_boxcare)
        #gm2.plt.plot(az[s], w_linear)
        #gm2.plt.show()
        
        if n == 1:
            gm2.plt.plot(az[s], w_boxcare, '.', markersize=2)
            gm2.plt.plot(az[s], w_linear, '.', markersize=2)
            gm2.plt.show()



        w_boxcar =((az[s]>(sphi+lower)/2.)&(az[s]<(upper+sphi)/2.))
        if lower > upper:
            w_boxcar =((az[s]>(sphi+lower)/2.)|(az[s]<(upper+sphi)/2.))

        if n < 2:
          gm2.plt.plot(az[s], w_boxcar,'.', markersize=2)
    gm2.plt.show()

    #      print "%03.1f %03.1f %02.0f %.1f " % (lower, upper, (upper-lower), az[s].shape[0]/1000.)


