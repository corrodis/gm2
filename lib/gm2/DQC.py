import psycopg2
from datetime import datetime
from gm2 import DB
from gm2 import util
from gm2 import np

class DQC(DB, object):
    """This class provides a set of function to query the g-2 dqc databank.

    Attributes:
        """
    def __init__(self, user="gm2_reader", host="g2db-priv", database="gm2_online_prod", port=5433):
        """Constructor connects to the online database.
        
        Args:
            user (str) : db user. Current default gm2_reader. 
            host(str) : db host. Default g2db-priv. 
            database(str) : db database. Default gm2_online_prod. 
            port(int) : db port. Defaults to 5433.
        """
        super(DQC, self).__init__(user=user, host=host, database=database, port=port)

    def load(self, field='t', quad=None, ctags=None, losses=None, fillcuts=None, start=None, end=None):
        where = ""
        if not field and not quad and not ctags and not losses and not fillcuts and not start and not end:
            where = ""
        else:
            where = " WHERE "
            where_n = 0
        if field:
            where += "gm2dq.subrun_status.field_ok = '%s'" % field
            where_n += 1
        if quad:
            if where_n > 0:
                where += " AND "
            where += "gm2dq.subrun_status.quad_ok = '%s'" % quad
            where_n += 1
        if ctags:
            if where_n > 0:
                where += " AND "
            where += "gm2dq.subrun_status.ctags_ok = '%s'" % ctags
            where_n += 1
        if losses:
            if where_n > 0:
                where += " AND "
            where += "gm2dq.subrun_status.losses_ok = '%s'" % losses
            where_n += 1
        if fillcuts:
            if where_n > 0:
                where += " AND "
            where += "gm2dq.subrun_status.fillcuts_ok = '%s'" % fillcuts
            where_n += 1

        if start:
            if where_n > 0:
                where += " AND "
            where += "start_gps >= '%s'" % start
            where_n += 1
        if end:
            if where_n > 0:
                where += " AND "
            where += "end_gps <= '%s'" % end
            where_n += 1
        quaery = """SELECT start_gps, end_gps FROM gm2dq.subrun_time INNER JOIN gm2dq.subrun_status ON gm2dq.subrun_status.run=gm2dq.subrun_time.run AND gm2dq.subrun_status.subrun=gm2dq.subrun_time.subrun %s;""" % where
        return self.query(quaery)

    def loadBetween(self, start_ts, end_ts, field='t', quad=None, ctags=None, losses=None, fillcuts=None):
        start = util.ts2datetime(np.array([start_ts]))[0].strftime("%Y-%m-%d %H:%M:%S.%f")
        end =   util.ts2datetime(np.array([end_ts]))[0].strftime("%Y-%m-%d %H:%M:%S.%f")
        return self.load(field, quad, ctags, losses, fillcuts, start, end)

    def select(self, times, field='t', quad=None, ctags=None, losses=None, fillcuts=None):
        data = self.loadBetween(times[0],times[-1],field, quad, ctags, losses, fillcuts)
        sel = times < 0
        for d in data:
            sel = sel | ((times > util.datetime2ts_dt(d[0]))&(times <  util.datetime2ts_dt(d[1])))
        return sel

