#!/usr/bin/env python

import matplotlib as mpl
mpl.use('Agg')

import gm2
from gm2 import plt, np
from gm2.plotutil import plot_ts
import datetime
import time

def timestamp(d):
    return time.mktime(d.timetuple()) * 1e9

import sys
if len(sys.argv) < 7:
    print "USAGE: fpJump run_no year month day hour min [sec]"
    raise SystemExit

no_start = int(sys.argv[1])
year  = int(sys.argv[2])
month = int(sys.argv[3])
day   = int(sys.argv[4])
hour  = int(sys.argv[5])
mins  = int(sys.argv[6])
if len(sys.argv) > 7:
  secs  = int(sys.argv[7])
else:
  secs  = 0

jumpt_t = datetime.datetime(year, month, day, hour, mins, secs)

#runs = np.arange(5807, 5835+1) #[5807]
runs = np.arange(no_start, no_start+2) #[5807]
fp = gm2.FixedProbe(runs, True)


def selectProbe(rad, layer):
    return (fp.id['rad'] == rad)&(fp.id['layer'] == layer)

fp_phi = fp.getPhi()

fp_time = fp.time
fp_freq = fp.freq

dtHalf  = datetime.timedelta(minutes=1)
dtOffset = datetime.timedelta(minutes=5) 

s_before = (fp_time[:,0] > timestamp(jumpt_t - dtOffset - dtHalf))&(fp_time[:,0] < timestamp(jumpt_t - dtOffset + dtHalf))
s_after =  (fp_time[:,0] > timestamp(jumpt_t + dtOffset - dtHalf))&(fp_time[:,0] < timestamp(jumpt_t + dtOffset + dtHalf))
f_before = np.nanmean(fp.freq[s_before,:], axis=0)
f_after  = np.nanmean(fp.freq[s_after,:], axis=0)

phi = (np.array([ord(i) for i in fp.id['yoke']]) - 65) * 30 - 15 + (fp.id['azi']-1) * 30/6. + 30/12.
#print phi
#print f_before.shape


layers = ["T", "B"]
radIds = ["O", "M", "I"]

figsize = [gm2.plt.rcParams['figure.figsize'][1] * 1.5, gm2.plt.rcParams['figure.figsize'][1] * 1]
plt.figure(figsize=figsize)
for radId in radIds:
    for layer in layers:
        s = selectProbe(radId, layer)
        plt.plot(phi[s], (f_after[s]-f_before[s])*gm2.HZ2PPM, '.', markersize=2, label=layer+"-"+radId)
plt.xlabel("azimuth [deg]")
plt.ylabel("field jump [ppm]")
plt.legend(markerscale=4)
plt.title("Run "+str(no_start)+" "+jumpt_t.strftime("%m/%d/%Y %H:%M"))
gm2.despine()
plt.savefig("plots/fpJump_"+str(year)+str(month)+str(day)+"_"+str(hour)+str(mins)+".png")
#plt.show()

