#!/usr/bin/env python

import matplotlib as mpl
mpl.use('Agg')

import gm2
from gm2 import plt, np
from gm2.plotutil import plot_ts

import sys
if len(sys.argv) < 2:
    print "USAGE: fpViewer run_no_start [run_no_end]"
    raise SystemExit

no_start = int(sys.argv[1])
if len(sys.argv) > 2:
    no_end = int(sys.argv[2])
else:
    no_end = no_start

#runs = np.arange(5807, 5835+1) #[5807]
runs = np.arange(no_start, no_end+1) #[5807]
fp = gm2.FixedProbe([], False)
fp.fname_path    = "TreeGenFixedProbe/fixedProbe_DAQ"
fp.loadFiles(list(runs))


def callback():
    return [fp.getTimeGPS(), fp.getFrequency(0), fp.getAmplitude(), fp.getPower(), fp.getFidLength()]
##_, fp_time, fp_freq = fp.getBasics()
fp_time, fp_freq, fp_amp, fp_power, fp_length  = fp.loop(callback)

fp_phi = fp.getPhi()

#fp_time = fp.time
#fp_freq = fp.freq

skip = 1

#ylim = 200.
#ylim2 = 50.

def selectStation(yoke, azi):
    return (fp.id['yoke'] == yoke)&(fp.id['azi'] == azi)

def selectProbe(rad, layer):
    return (fp.id['rad'] == rad)&(fp.id['layer'] == layer)

layers = ["T", "B"]
radIds = ["O", "M", "I"]

alpha = 1.0
figsize = [gm2.plt.rcParams['figure.figsize'][1] * 2.0, gm2.plt.rcParams['figure.figsize'][1] * 1.5]
from matplotlib.dates import DateFormatter
formatter = DateFormatter('%m/%d\n%H:%M')

# create folder for pngs
import os
dirname = str(runs[0])+"to"+str(runs[-1])
try:
    os.mkdir("plots/")
except:
    pass
try:
    os.mkdir("plots/"+dirname)
except:
    pass

test = 0
for yoke_ in np.arange(ord('A'), ord('L')+1):
    yoke = chr(yoke_)
    for aziId in np.arange(1,6+1):
        print("Yoke "+yoke," : "+str(aziId))
        if test:
            break
        s_station = selectStation(yoke, aziId)
        #print("Station", np.argwhere(s_station).shape)
        #freq_mean = 0
        #freq_n    = 0
        f = plt.figure(figsize=figsize)
        ax1 = plt.subplot(211)
        for radId in radIds:
            for layer in layers:
                s = selectProbe(radId, layer)&s_station
                #print("Number", np.argwhere(s).shape)
                probe = np.argwhere(s)
                if len(probe) == 1:
                    #mean = fp_freq[skip:, s].mean()
                    probe = probe[0]
                    label_ = layer
                    if radId in ["I"]:
                        label_ += radId+"  "
                    else:
                        label_ += radId
                    phi_ = fp_phi[s]
                    label_ += (r"#%03i"  % (probe))
                    s_t = fp_time[skip:, s] > 0
                    if len(fp_time[skip:, s][s_t]) > 0:
                        #if freq_n == 0:
                        #    freq_mean = fp_freq[:, s]-mean
                        #else:
                        #    freq_mean += fp_freq[:, s]-mean
                        #freq_n += 1
                        plot_ts(fp_time[skip:, s][s_t], fp_power[skip:, s][s_t], '.', markersize=2, label=label_, alpha=alpha)
        plt.ylabel(r'power')
        #plt.ylim([-ylim, ylim])

        plt.title("Yoke "+(yoke)+", azi "+str(aziId)+(" at %.0f" % ((phi_[0]+360)%360))  )

        #if freq_n > 0:
            #freq_mean /= freq_n
        plt.subplot(212, sharex=ax1)
        for radId in radIds:
            for layer in layers:
                s = selectProbe(radId, layer)&s_station
                probe = np.argwhere(s)
                if len(probe) == 1:
                    #mean = fp_freq[skip:, s].mean()
                    s_t = fp_time[skip:, s] > 0
                    if len(fp_time[skip:, s][s_t]) > 0:
                        #print fp_power[skip:, s][s_t]
                        plot_ts(fp_time[skip:, s][s_t], fp_length[skip:, s][s_t], '.', markersize=2, label=label_, alpha=alpha)

        plt.xlabel("time")
        plt.ylabel('length')
        #plt.ylim([-ylim2, ylim2])

        '''
        plt.subplot(313, sharex=ax1)
        for radId in radIds:
            for layer in layers:
                s = selectProbe(radId, layer)&s_station
                probe = np.argwhere(s)
                if len(probe) == 1:
                    #mean = fp_freq[skip:, s].mean()
                    s_t = fp_time[skip:, s] > 0
                    if len(fp_time[skip:, s][s_t]) > 0:
                        plot_ts(fp_time[skip:, s][s_t], fp_length[skip:, s][s_t], '.', markersize=2, label=label_, alpha=alpha)
        
        plt.xlabel("time")
        plt.ylabel('length [s]')
        #plt.ylim([-ylim3, ylim3])
        '''
        plt.gca().xaxis.set_major_formatter(formatter)
        #ax.xaxis.set_tick_params(rotation=30, labelsize=10)
        gm2.despine()
        plt.subplot(211)
        plt.setp(ax1.get_xticklabels(), visible=False)

        #plt.subplot(312)
        #plt.setp(ax1.get_xticklabels(), visible=False)
        #plt.gca().set_xticklabels([]);
        lgnd = plt.legend(loc='upper center', bbox_to_anchor=(0.2, -.22, 0.6, 0.2), ncol=3, fontsize=12)
        for lgndHandl in lgnd.legendHandles:
            lgndHandl._legmarker.set_markersize(12)
        f.savefig("plots/"+dirname+"/fpsig_"+yoke+"_"+str(aziId)+".png")
        #plt.show()
        #test = 1

# combine plots to gif
os.system("convert -delay 100 plots/"+dirname+"/fpsig*_*.png -loop 0 plots/fpsig_"+dirname+".gif")
print("plots/fpsig_"+dirname+".gif")

